<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Region;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            ['name' => 'Andijon viloyati'],
            ['name' => 'Buxoro viloyati'],
            ['name' => 'Farg\'ona viloyati'],
            ['name' => 'Jizzax viloyati'],
            ['name' => 'Xorazm viloyati'],
            ['name' => 'Namangan viloyati'],
            ['name' => 'Navoiy viloyati'],
            ['name' => 'Qashqadaryo viloyati'],
            ['name' => 'Qoraqalpog\'iston respublikasi'],
            ['name' => 'Samarqand viloyati'],
            ['name' => 'Sirdaryo viloyati'],
            ['name' => 'Surxondaryo viloyati'],
            ['name' => 'Toshkent viloyati'],
            ['name' => 'Toshkent shahri'],
        ];
        Region::insert($regions);
    }
}
