<div class="axil-product-area bg-color-white axil-section-gap pb--50 pb_sm--30">
    <div class="container">
        <div class="section-title-wrapper">
            <span class="title-highlighter highlighter-primary"><i class="far fa-shopping-basket"></i> Sizga yoqishi mumkin</span>
            <h2 class="title">O'xshash mahsulotlar</h2>
        </div>
        <div class="recent-product-activation slick-layout-wrapper--15 axil-slick-arrow arrow-top-slide">
            @foreach ($similar_products as $item)
            <div class="axil-product">
                <div class="thumbnail">
                    <a href="{{ route('product.show',  $item->id) }}">
                        <img data-sal="zoom-out" data-sal-delay="200" data-sal-duration="800" class="main-img" src="{{ $item->image }}" style="width: 300px; height: 300px;" alt="Product Images">
                        <img class="hover-img" src="{{ $item->image }}" style="width: 300px; height: 300px;" alt="Product Images">
                    </a>
                    <div class="product-hover-action">
                        <ul class="cart-action">
                            <li class="quickview">
                                <a href="{{ route('product.show',  $item->id) }}">
                                    <i class="far fa-eye"></i>
                                </a>
                            </li>
                            <li class="select-option">
                                <a href="{{ route('product.show',  $item->id) }}">
                                    Buyurtma berish
                                </a>
                            </li>
                            <li class="wishlist"><a href="wishlist.html"><i class="far fa-heart"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="product-content">
                    <div class="inner">
                        <div class="product-rating">
                            <span class="icon">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </span>
                            <span class="rating-number">(64)</span>
                        </div>
                        <h5 class="title">
                            <a href="{{ route('product.show',  $item->id) }}">{{ $item->name }}</a>
                        </h5>
                        <div class="product-price-variant">
                            <span class="price current-price">{{ number_format($item->price, 0, ' ', ' ') }} so'm</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</div>
