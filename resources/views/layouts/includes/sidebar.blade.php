<aside class="axil-dashboard-aside">
    <nav class="axil-dashboard-nav">
        <div class="nav nav-tabs" role="tablist">
            <a class="nav-item nav-link active" data-bs-toggle="tab" href="{{ route('customer.orders') }}" role="tab" aria-selected="false"><i class="fas fa-shopping-basket"></i>Mening buyurmalarim</a>
            <a class="nav-item nav-link" data-bs-toggle="tab" href="#nav-orders" role="tab" aria-selected="false"><i class="fas fa-heart"></i>Mening sevimlilarim</a>
            <a class="nav-item nav-link" data-bs-toggle="tab" href="#nav-account" role="tab" aria-selected="false"><i class="fas fa-user"></i>Profil ma'lumotlari</a>
            <a class="nav-item nav-link"href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fal fa-sign-out"></i>
                <span class="align-middle">Chiqish</span>
            </a>
        </div>
        
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </nav>
</aside>
