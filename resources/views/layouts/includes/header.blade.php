<header class="header axil-header header-style-1">
    <div class="axil-header-top" style="background-color: #f9f3f0">
        <div class="container">
            {{-- <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="header-top-dropdown">
                        <div class="dropdown">
                            <button class="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                English
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">English</a></li>
                                <li><a class="dropdown-item" href="#">Arabic</a></li>
                                <li><a class="dropdown-item" href="#">Spanish</a></li>
                            </ul>
                        </div>
                        <div class="dropdown">
                            <button class="dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                USD
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">USD</a></li>
                                <li><a class="dropdown-item" href="#">AUD</a></li>
                                <li><a class="dropdown-item" href="#">EUR</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="header-top-link">
                        <ul class="quick-link">
                            <li><a href="#">Help</a></li>
                            <li><a href="register">Join Us</a></li>
                            <li><a href="{{ route('admin.login') }}">Sign In</a></li>
                        </ul>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
    <!-- Start Mainmenu Area  -->
    <div id="axil-sticky-placeholder"></div>
    <div class="axil-mainmenu">
        <div class="container">
            <div class="header-navbar" style="padding: 15px 40px;">
                <div class="header-brand">
                    <a href="/" class="logo logo-dark">
                        <img src="{{ asset('themes/images/logol.jpg') }}" alt="Site Logo">
                    </a>
                    <a href="/" class="logo logo-light">
                        <img src="{{ asset('themes/images/logo.jpg') }}" alt="Site Logo">
                    </a>
                </div>
                <div class="header-main-nav">
                    <!-- Start Mainmanu Nav -->
                    <nav class="mainmenu-nav pb--10 pt--10">
                        <button class="mobile-close-btn mobile-nav-toggler"><i class="fas fa-times"></i></button>
                        <div class="mobile-nav-brand">
                            <a href="#" class="logo">
                                <img src="{{ asset('themes/shop/assets/images/logo/logo.png') }}" alt="Site Logo">
                            </a>
                        </div>
                        {{-- <ul class="mainmenu">
                                <li class="menu-item-has-children">
                                    <a href="#">Home</a>
                                    <ul class="axil-submenu">
                                        <li><a href="index-1.html">Home - Electronics</a></li>
                                        <li><a href="index-2.html">Home - NFT</a></li>
                                        <li><a href="index-3.html">Home - Fashion</a></li>
                                        <li><a href="index-4.html">Home - Jewellery</a></li>
                                        <li><a href="index-5.html">Home - Furniture</a></li>
                                        <li><a href="index-6.html">Home - Multipurpose</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Shop</a>
                                    <ul class="axil-submenu">
                                        <li><a href="shop-sidebar.html">Shop With Sidebar</a></li>
                                        <li><a href="shop.html">Shop no Sidebar</a></li>
                                        <li><a href="single-product.html">Product Variation 1</a></li>
                                        <li><a href="single-product-2.html">Product Variation 2</a></li>
                                        <li><a href="single-product-3.html">Product Variation 3</a></li>
                                        <li><a href="single-product-4.html">Product Variation 4</a></li>
                                        <li><a href="single-product-5.html">Product Variation 5</a></li>
                                        <li><a href="single-product-6.html">Product Variation 6</a></li>
                                        <li><a href="single-product-7.html">Product Variation 7</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#">Pages</a>
                                    <ul class="axil-submenu">
                                        <li><a href="wishlist.html">Wishlist</a></li>
                                        <li><a href="cart.html">Cart</a></li>
                                        <li><a href="checkout.html">Checkout</a></li>
                                        <li><a href="my-account.html">Account</a></li>
                                        <li><a href="sign-up.html">Sign Up</a></li>
                                        <li><a href="sign-in.html">Sign In</a></li>
                                        <li><a href="forgot-password.html">Forgot Password</a></li>
                                        <li><a href="reset-password.html">Reset Password</a></li>
                                        <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                        <li><a href="coming-soon.html">Coming Soon</a></li>
                                        <li><a href="404.html">404 Error</a></li>
                                        <li><a href="typography.html">Typography</a></li>
                                    </ul>
                                </li>
                                <li><a href="about-us.html">About</a></li>
                                <li class="menu-item-has-children">
                                    <a href="#">Blog</a>
                                    <ul class="axil-submenu">
                                        <li><a href="blog.html">Blog List</a></li>
                                        <li><a href="blog-grid.html">Blog Grid</a></li>
                                        <li><a href="blog-details.html">Standard Post</a></li>
                                        <li><a href="blog-gallery.html">Gallery Post</a></li>
                                        <li><a href="blog-video.html">Video Post</a></li>
                                        <li><a href="blog-audio.html">Audio Post</a></li>
                                        <li><a href="blog-quote.html">Quote Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul> --}}
                    </nav>
                    <!-- End Mainmanu Nav -->
                </div>
                <div class="header-action">
                    <ul class="action-list">
                        <li class="axil-search">
                            <a href="javascript:void(0)" class="header-search-icon" title="Search">
                                <i class="flaticon-magnifying-glass"></i>
                            </a>
                        </li>
                        @auth
                        <li class="wishlist">
                            <a href="{{ route('favorite.index') }}">
                                <i class="flaticon-heart"></i>
                            </a>
                        </li>
                        @endauth

                        <li class="my-account">
                            <a href="javascript:void(0)">
                                <i class="flaticon-person"></i>
                            </a>
                            <div class="my-account-dropdown">
                                @auth
                                     <span class="title">{{   auth()->user()->fullname }} </span>
                                @endauth

                                <ul>
                                    <li>
                                        <a href="{{ route('account.index') }}">Profilim</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.dashboard') }}">Adminlar uchun</a>
                                    </li>
                                    <li>
                                        <a href="/contact">Aloqa uchun</a>
                                    </li>
                                </ul>
                                @guest
                                    <div class="login-btn">
                                        <a href="{{ route('login') }}" class="axil-btn btn-bg-primary">Kirish</a>
                                    </div>
                                    <div class="reg-footer text-center">
                                        <p class="title" style="margin-bottom: 10px">Akkount yo'qmi?</p>
                                        <a href="{{ route('register') }}" class="btn-link">Ro'yxatdan o'ting</a>
                                    </div>
                                @endguest
                                @auth
                                <div class="login-btn">

                                        <form method="POST" action="{{ route('logout') }}">
                                        @csrf

                                        <button type="submit" class="axil-btn btn-bg-primary">
                                            Chiqish
                                        </button>
                                    </form>
                                </div>
                                @endauth
                            </div>
                        </li>
                        {{-- <li class="axil-mobile-toggle">
                            <button class="menu-btn mobile-nav-toggler">
                                <i class="flaticon-menu-2"></i>
                            </button>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Mainmenu Area -->
</header>
