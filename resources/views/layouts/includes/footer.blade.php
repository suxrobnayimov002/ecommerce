<footer class="axil-footer-area footer-style-2">
    <!-- Start Footer Top Area  -->
    <div class="footer-top separator-top">
        <div class="container">
            <div class="row">
                <!-- Start Single Widget  -->
                <div class="col-lg-3 col-sm-6">
                    <div class="axil-footer-widget">
                        <h5 class="widget-title">Qo'llab quvvatlash</h5>
                        <!-- <div class="logo mb--30">
                        <a href="index.html">
                            <img class="light-logo" src="{{ asset('themes/shop/assets/images/logo/logo.png') }}" alt="Logo Images">
                        </a>
                    </div> -->
                        <div class="inner">
                            <p>Chilonzor, Toshkent
                            </p>
                            <ul class="support-list-item">
                                <li><a href="mailto:example@domain.com"><i class="fal fa-envelope-open"></i> osavdo@gmail.com</a></li>
                                <li><a href="tel:(+01)850-315-5862"><i class="fal fa-phone-alt"></i> +998 (71) 123 45 67</a></li>
                                <!-- <li><i class="fal fa-map-marker-alt"></i> 685 Market Street,  <br> Las Vegas, LA 95820, <br> United States.</li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Widget  -->
                <!-- Start Single Widget  -->
                <div class="col-lg-3 col-sm-6">
                    <div class="axil-footer-widget">
                        <h5 class="widget-title">Profil</h5>
                        <div class="inner">
                            <ul>
                                <li><a href="my-account.html">Mening profilim</a></li>
                                @guest<li><a href="{{ route('login') }}">Kirish/Ro'yxatdan o'tish</a></li>@endguest
                                <li><a href="{{ route('wishlist.index') }}">Sevimlilarim</a></li>
                                <li><a href="{{ route('customer.orders') }}">Buyurtmalarim</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Widget  -->
                <!-- Start Single Widget  -->
                {{-- <div class="col-lg-3 col-sm-6">
                    <div class="axil-footer-widget">
                        <h5 class="widget-title">Quick Link</h5>
                        <div class="inner">
                            <ul>
                                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                <li><a href="terms-of-service.html">Terms Of Use</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="contact.html">Contact</a></li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div> --}}
                <!-- End Single Widget  -->
                <!-- Start Single Widget  -->
                {{-- <div class="col-lg-3 col-sm-6">
                    <div class="axil-footer-widget">
                        <h5 class="widget-title">Download App</h5>
                        <div class="inner">
                            <span>Save $3 With App & New User only</span>
                            <div class="download-btn-group">
                                <div class="qr-code">
                                    <img src="{{ asset('themes/shop/assets/images/others/qr.png') }}" alt="Axilthemes">
                                </div>
                                <div class="app-link">
                                    <a href="#">
                                        <img src="{{ asset('themes/shop/assets/images/others/app-store.png') }}" alt="App Store">
                                    </a>
                                    <a href="#">
                                        <img src="{{ asset('themes/shop/assets/images/others/play-store.png') }}" alt="Play Store">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!-- End Single Widget  -->
            </div>
        </div>
    </div>
    <!-- End Footer Top Area  -->
    <!-- Start Copyright Area  -->
    <div class="copyright-area copyright-default separator-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-4">
                    <div class="social-share">
                        <a href="https://instagram.com/_osavdo_?igshid=YmMyMTA2M2Y=" target="_blank"><i class="fab fa-instagram"></i></a>
                        <a href="https://t.me/Maxsulotlar_Online" target="_blank"><i class="fab fa-telegram"></i></a>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div class="copyright-left d-flex flex-wrap justify-content-center">
                        <ul class="quick-link">
                            <li>© 2022. Barcha huquqlar himoyalangan <a target="_blank" href="#">oSavdo</a>.</li>
                        </ul>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    <!-- End Copyright Area  -->
</footer>
