@extends('layouts.product')
@section('content')
<main class="main-wrapper">
    <!-- Start Breadcrumb Area  -->
    <div class="axil-breadcrumb-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-8">
                    <div class="inner">
                        <ul class="axil-breadcrumb">
                            <li class="axil-breadcrumb-item"><a href="/">Bosh sahifa</a></li>
                            <li class="separator"></li>
                            <li class="axil-breadcrumb-item active" aria-current="page">Barcha mahsulotlar</li>
                        </ul>
                        <h1 class="title">Barcha mahsulotlar bilan tanishing</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb Area  -->
    <!-- Start Shop Area  -->
    <div class="axil-shop-area axil-section-gap bg-color-white">
        <div class="container">
            <div class="row mb--40">
                <div class="col-lg-12">
                    <div class="axil-shop-top">
                        <form action="{{ route('product.index') }}" method="get">
                            <div class="row align-items-baseline">
                                <div class="col-lg-8">
                                    <div class="category-select">
                                        <div class="row w-100">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="search" class="form-control" name="product_name" id="prod-search" placeholder="Qidiruv..." style="border: 2px solid #CBD3D9; font-weight: 500; height: 55px;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <!-- Start Single Select  -->
                                                <select class="single-select w-100" style="margin: 0" name="category_id">
                                                    <option value="">Kategoriyalarni tanlang...</option>
                                                    @foreach($categories as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 d-flex">
                                    <div class="category-select mt_md--10 mt_sm--10 justify-content-lg-end">
                                        <!-- Start Single Select  -->
                                        <select class="single-select w-100" >
                                            <option value="1">Mahsulotlar bo'yicha...</option>
                                            <option value="2">Yangi mahsulotlar</option>
                                            <option value="3">Avval arzonlari</option>
                                            <option value="4">Avval qimmatlari</option>
                                        </select>
                                        <!-- End Single Select  -->
                                    </div>
                                    <div style="margin-left: 20px">
                                        <button type="submit" class="axil-btn btn-bg-primary" style="height: 55px;"><i class="far fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row row--15">
                @foreach ($products as $item )
                <div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb--30">
                    <div class="axil-product product-style-one">
                        <div class="thumbnail">
                            <a href="{{ route('product.show',  $item->id) }}" class="mobile-product">
                                <img data-sal="zoom-out" data-sal-delay="200" data-sal-duration="800" class="main-img" src="{{ $item->image }}" style="width: 300px; height: 300px;" alt="Product Images">
                                <img class="hover-img" src="{{ $item->image }}" style="width: 300px; height: 300px;" alt="Product Images">
                            </a>
                            <div class="product-hover-action">
                                <ul class="cart-action">
                                    <li class="quickview">
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#quick-view-modal"><i class="far fa-eye"></i></a>
                                    </li>
                                    <li class="select-option">
                                        <a href="{{ route('product.show',  $item->id) }}">Buyurtma berish</a>
                                    </li>
                                    <li class="wishlist">
                                        <a href="prevent()"  data-id="{{  $item->id }}" id="addToWishlist"><i class="far fa-heart"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="inner">
                                <div class="product-rating">
                                    <span class="icon">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </span>
                                    <span class="rating-number">(64)</span>
                                </div>
                                <h5 class="title">
                                    <a href="{{ route('product.show',  $item->id) }}">{{ $item->name }}</a>
                                </h5>
                                <div class="product-price-variant">
                                    <span class="price current-price">{{ number_format($item->price, 0, ' ', ' ') }} so'm</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{-- <div class="text-center pt--30">
                <a href="#" class="axil-btn btn-bg-lighter btn-load-more">Yanada ko'proq</a>
            </div> --}}
        </div>
        <!-- End .container -->
    </div>
    <!-- End Axil Newsletter Area  -->
</main>
@endsection

<script>
    $(document).on("click", "#addToWishlist", function () {


          event.preventDefault();
          var productID = $(this).data('id');

          addWishlist(productID);
          // $('#streamInfo').modal('show');
          // As pointed out in comments,
          // it is unnecessary to have to manually call the modal.
          // $('#addBookDialog').modal('show');
      });
    function addWishlist(productID){


      $.ajax('/product-wishlist/'+productID, {
      type: 'POST',  // http method
      data: {
          _token: "{{ csrf_token() }}"
      },  // data to submit
      success: function (data, status, xhr) {
      console.log(data);

      },
      error: function (jqXhr, textStatus, errorMessage) {
              $('p').append('Error' + errorMessage);
      }
      });
      }
</script>
