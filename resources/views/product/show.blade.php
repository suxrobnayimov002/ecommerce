@extends('layouts.product')
@section('content')
<main class="main-wrapper">
    <!-- Start Shop Area  -->
    <div class="axil-single-product-area bg-color-white">
        <div class="single-product-thumb axil-section-gap pb_sm--0 bg-vista-white" style="padding-top: 50px">
            <div class="container">
                <a href="/" class="mb--40">
                    <i class="fal fa-long-arrow-left" style="padding-right: 10px"></i>
                    Orqaga qaytish
                </a>
                <h2 class="product-title mt--20">{{ $products->name }}</h2>
                <div class="row row--25 productshow-items">
                    <div class="col col-6 mb--40 product-show-list mobile-show" style="display: flex; justify-content: center; align-items: center;">
                        <div class="h-100">
                            <div class="position-sticky sticky-top">
                                <div class="row row--10">
                                    <div class="col-12 mb--20">
                                        <div class="single-product-thumbnail axil-product thumbnail-grid">
                                            <div class="thumbnail d-flex align-items-center justify-center">
                                                <img class="img-fluid" src="{{ $products->image }}" style="width: 400px; height: 400px;" alt="Product Images">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-6 mb--40 product-show-list">
                        <div class="h-100">
                            <div class="position-sticky sticky-top">
                                <div class="single-product-content p-4">
                                    <h4 class="primary-color mb--20 mt--10 desc-heading">Buyurtma berish</h4>
                                    <div class="inner">
                                        <ul class="d-flex align-items-center " style="list-style: none; padding: 0;">
                                            <li class="d-flex align-items-center">
                                                <span class="price-amount" style="margin-bottom: 0; padding-left: 0;">Mahsulot narxi: <b style="padding-left: 10px"> {{ number_format($products->price, 0, ' ', ' ') }} so'm</b></span>
                                            </li>
                                            <li class="wishlist" style="margin-left: 20px;"><a href="prevent()"  data-id="{{  $products->id }}" id="addToWishlist" class="axil-btn wishlist-btn"><i class="far fa-heart"></i></a></li>
                                        </ul>
                                        <!-- End Product Action Wrapper  -->

                                        <div class="product-desc-wrapper pt--10 pt_sm--60">
                                            <div class="axil-checkout-billing">
                                                <form action="{{ route('order.store') }}" method="post">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Ismingiz <span>*</span></label>
                                                                <input type="text" id="first-name" name="customer_name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input name="product_id" type="hidden" value="{{ $products->id }}"/>
                                                    <div class="form-group">
                                                        <label>Telefon raqami<span>*</span></label>
                                                        <input type="tel" id="phone" name="phone_number">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Yashash manzilingiz<span>*</span></label>
                                                        <select id="RegionId" name="region_id" >
                                                            <option value="">Tanlang...</option>
                                                            @foreach ($regions as $region)
                                                                <option value="{{ $region->id }}"> {{ $region->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="align-center">
                                                        <button type="submit" class="axil-btn btn-bg-primary">Buyurtma berish</button>


                                                    </div>
                                                </form>
                                            </div>
                                            <!-- End .pro-des-features -->
                                        </div>
                                        <!-- End .product-desc-wrapper -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-white">
            <div class="container">
                <div class="row p-3">
                    <div class="col-xl-12">
                      <div class="nav-align-top mb-4">
                        <ul class="nav nav-tabs nav-pills row" role="tablist">
                          <li class="nav-item col-xl-4">
                                <button
                                type="button"
                                class="nav-link active p-4"
                                role="tab"
                                data-bs-toggle="tab"
                                data-bs-target="#navs-pills-top-home"
                                aria-controls="navs-pills-top-home"
                                aria-selected="true"
                            >
                                Batafsil ma'lumot
                            </button>
                          </li>
                          {{-- <li class="nav-item col-xl-4">
                            <button
                              type="button"
                              class="nav-link p-4"
                              role="tab"
                              data-bs-toggle="tab"
                              data-bs-target="#navs-pills-top-profile"
                              aria-controls="navs-pills-top-profile"
                              aria-selected="false"
                                disabled
                            >
                              Xaridorlar fikri
                            </button>
                          </li> --}}
                        </ul>
                      </div>
                    </div>
                </div>
                <div class="tab-content bg-white">
                    <div class="tab-pane fade show active" id="navs-pills-top-home" role="tabpanel">
                        <p>{{ $products->description }}</p>
                    </div>
                    <div class="tab-pane fade" id="navs-pills-top-profile" role="tabpanel">
                        <p>
                          Donut dragée jelly pie halvah. Danish gingerbread bonbon cookie wafer candy oat cake ice
                          cream. Gummies halvah tootsie roll muffin biscuit icing dessert gingerbread. Pastry ice cream
                          cheesecake fruitcake.
                        </p>
                        <p class="mb-0">
                          Jelly-o jelly beans icing pastry cake cake lemon drops. Muffin muffin pie tiramisu halvah
                          cotton candy liquorice caramels.
                        </p>
                      </div>
                </div>
            </div>
        </div>
        <!-- End .single-product-thumb -->
        {{-- {{ $products->links() }} --}}
    </div>
    <!-- End Shop Area  -->

    <!-- Start Recently Viewed Product Area  -->
    @include('layouts.includes.similar_products', ['similar_products' => $similar_products])
    <!-- End Recently Viewed Product Area  -->
</main>
@endsection
