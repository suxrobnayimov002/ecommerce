@extends('layouts.admin')

@section('title', 'Product')

@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-header">
                <h4>Mahsulotni tahrirlash</h4>
            </div>
            <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data"
                class="card-body">
                @csrf()
                @method('PUT')
                <div class="mb-3">
                    <label for="exampleDataList" class="form-label">Mahsulot kategoriyasi</label>
                    <select class="form-select" id="inputGroupSelect01"  name="category_id">
                        @foreach($categories as $item)
                            @if($item->id == $product->category_id)
                            <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                            @else
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <span class="text-danger">{{ $errors->first('category_id') }}</span>
                <div class="mb-3">
                    <label class="col-sm-1 col-form-label" for="basic-default-name">Mahsulot nomi</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="name" value="{{ $product->name }}" id="basic-default-name"
                            @error('name') is-invalid @else is-valid @enderror" placeholder="Category Name" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('name') }}</span>
                <div>
                    <label for="exampleFormControlTextarea1" class="form-label">Mahsulot haqida batafsil ma'lumot</label>
                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"
                        @error('description') is-invalid @else is-valid @enderror">{{ $product->description }}</textarea>
                </div>
                <span class="text-danger">{{ $errors->first('name') }}</span>
                <div class="mb-3">
                    <label for="formFileMultiple" class="form-label">Mahsulot rasmi</label>
                    <input class="form-control" type="file" name="image" value="{{ $product->image }}" id="formFileMultiple" multiple
                        @error('image') is-invalid @else is-valid @enderror" />
                </div>
                <span class="text-danger">{{ $errors->first('image') }}</span>
                <div class="mb-3">
                    <label for="html5-number-input" class="col-md-2 col-form-label">Narxi</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="price" value="{{ $product->price }}" placeholder="123456.."
                            id="html5-number-input" @error('price') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('price') }}</span>
                <div class="mb-3">
                    <label for="html5-number-input" class="col-md-2 col-form-label">Mahsulotdan olinadigan foyda</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="profit" value="{{ $product->profit }}"
                            id="html5-number-input" @error('profit') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('advertising_link') }}</span>
                <div class="mb-3">
                    <label for="advertising_link" class="form-label">Reklama posti uchun link</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="advertising_link" value="{{ $product->advertising_link }}" id="advertising_link" @error('advertising_link') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('coin') }}</span>
                <div class="mb-3">
                    <label for="coin" class="form-label">Mahsulot uchun COIN miqdorini kiriting</label>
                    <div class="col-md-12">
                        <input class="form-control" type="number" min="0" name="coin" value="{{ $product->coin }}"  id="coin" @error('coin') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('is_published') }}</span>
                <div class="mb-3">
                    <label for="exampleDataList" class="form-label">Mahsulot holati</label>
                    <select class="form-select" id="inputGroupSelect01" name="is_published">
                        <option value="">Tanlang...</option>
                        <option value="1" {!! $product->is_published ? ' selected="selected"' : ''!!}>Faol</option>
                        <option value="0" {!! !$product->is_published ? ' selected="selected"' : ''!!}>Faol emas</option>
                      </select>
                </div>
                <button type="submit" class="btn btn-primary btn-md" style="float: right !important;">
                    <i class="menu-icon tf-icons bx bx-check"></i>
                    {{ __('Ma\'lumotni saqlash') }}
                </button>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@stop
