@extends('layouts.admin')

@section('title', 'Product')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header">
            <h4>Mahsulot qo'shish</h4>
        </div>
        <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data" class="card-body">
            @csrf()
                <div class="mb-3">
                    <label for="exampleDataList" class="form-label">Mahsulot kategoriyasi</label>
                    <select class="form-select" id="inputGroupSelect01" name="category_id">
                        <option value="">Kategoriyani tanlang...</option>
                        @foreach($categories as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <span class="text-danger">{{ $errors->first('category_id') }}</span>
                <div class="mb-3">
                    <label class="col-sm-2 col-form-label" for="basic-default-name">Mahsulot nomi</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="name" id="basic-default-name"  @error('name') is-invalid @else is-valid @enderror" placeholder="Mahsulot nomini shu yerga kiriting" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('name') }}</span>
                <div>
                    <label for="exampleFormControlTextarea1" class="form-label">Mahsulot haqida batafsil ma'lumot</label>
                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" @error('description') is-invalid @else is-valid @enderror"></textarea>
                </div>
                <span class="text-danger">{{ $errors->first('description') }}</span>
                <div class="mb-3 mt-3">
                    <label for="formFileMultiple" class="form-label">Mahsulot rasmi</label>
                    <input class="form-control" type="file" name="image" id="formFileMultiple" multiple @error('image') is-invalid @else is-valid @enderror" />
                </div>
                <span class="text-danger">{{ $errors->first('image') }}</span>
                <div class="mb-3">
                    <label for="html5-number-input" class="col-md-2 col-form-label">Narxi</label>
                    <div class="col-md-12">
                      <input class="form-control" type="number" name="price" placeholder="450000" id="html5-number-input" @error('price') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('price') }}</span>
                <div class="mb-3">
                    <label for="html5-number-input" class="col-md-2 col-form-label">Profit</label>
                    <div class="col-md-12">
                      <input class="form-control" type="number" name="profit" placeholder="30000" id="html5-number-input" @error('profit') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('advertising_link') }}</span>
                <div class="mb-3">
                    <label for="advertising_link" class="form-label">Reklama posti uchun link</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="advertising_link" placeholder="Telegramdan olingan linkni kiriting" id="advertising_link" @error('advertising_link') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('coin') }}</span>
                <div class="mb-3">
                    <label for="coin" class="form-label">Mahsulot uchun COIN miqdorini kiriting</label>
                    <div class="col-md-12">
                        <input class="form-control" type="number" min="0" name="coin" placeholder="COIN miqdorini kiriting" id="coin" @error('coin') is-invalid @else is-valid @enderror" />
                    </div>
                </div>
                <span class="text-danger">{{ $errors->first('profit') }}</span>
                <div class="mb-3">
                    <label for="exampleDataList" class="form-label">Mahsulot holati</label>
                    <select class="form-select" id="inputGroupSelect01" name="is_published">
                        <option value="">Tanlang...</option>
                        <option value="1">Faol</option>
                        <option value="0">Faol emas</option>
                      </select>
                </div>
                <button type="submit" class="btn btn-primary btn-md" style="float: right">
                    <i class="menu-icon tf-icons bx bx-check"></i>
                    {{ __('Ma\'lumotni saqlash') }}
                </button>
        </form>
    </div>

</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

</script>
@stop
