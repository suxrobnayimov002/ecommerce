@extends('layouts.admin')

@section('title', 'Product')

@section('content_header')
<h4>Product</h4>
@stop

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-lists-center">
            <h4>Mahsulotlar ro'yxati</h4>
            <h3 class="card-title d-flex">
                <a href="{{ route('products.create') }}" class="btn btn-primary" style="float: right"><span class="menu-icon tf-icons bx bx-plus"></span>Mahsulot qo'shish
                </a>
            </h3>
        </div>
        <div class="card-body">
            <div class="">
                <table cellpadding="0" cellspacing="0" id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Mahsulot kategoriyasi</th>
                            <th>Mahsulot nomi</th>
                            <th>Mahsulot rasmi</th>
                            <th>Narxi</th>
                            <th>Adminlar uchun foyda</th>
                            <th>Mahsulot holati</th>
                            <th>Amallar</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i = 1;?>
                        @foreach($products as $item)
                        <tr style="height: 150px !important;">
                            <td>{{ $i++ }}</td>
                            <td>{{ $item->category->name }} </td>
                            <td>{{ $item->name }} </td>
                            <td><img src="{{ $item->image }}" alt=""style="width:120px; height: 80px"></td>
                            <td>{{ number_format($item->price, 0, ' ', ' ' ) }} so'm</td>
                            <td>{{ number_format($item->profit, 0, ' ', ' ') }} so'm</td>
                            <td>{{ ($item->is_published) ? 'Faol' : 'Faol emas' }} </td>
                            <td class="d-flex flex-wrap" style="align-items: center; height: 100%;">
                                <a href="{{ route('products.edit',  $item->id) }}" class="btn btn-info btn-sm mb-2" style="margin-right: 20px; width: 100px;">
                                    <i class="menu-icon tf-icons bx bx-edit-alt"></i>
                                    <span class="">Tahrirlash</span>
                                </a>
                                {{-- <form action="{{ route('products.destroy',  $item->id) }}" method="post" onsubmit="return confirm('Сиз ростдан ҳам ушбу маълумотни ўчиришни хохлайсизми ?')">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-sm btn-danger" style="width: 100px">
                                        <i class="menu-icon tf-icons bx bx-trash"></i>
                                        <span class="">O'chirish</span>
                                    </button>
                                </form> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- {{ $products->links() }} --}}
</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

</script>
@stop
