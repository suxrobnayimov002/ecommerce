@extends('layouts.admin')

@section('title', 'Offer')

@section('content')
<div class="content-wrapper">
    <div class="container-xxl flex-grow-1 container-p-y">
        <form action="{{ route('admin.offer.index') }}" method="get">
        <div class="row" style="margin-bottom: 30px">
            <div class="col-md-3 col-lg-16 d-flex align-items-center">
                <h4>Barcha offerlar ro'yxati</h4>
            </div>

            <div class="col-md-4 col-lg-4 mb-3">
                <div class="input-group input-group-merge">
                    <span class="input-group-text" id="basic-addon-search31"><i class="bx bx-search" style="font-size: 19px"></i></span>
                    <input
                    id="search"
                    type="text"
                    name="product_name"
                    value="{{ request()->input('search') }}"
                    class="form-control"
                    placeholder="Qidiruv..."
                    aria-label="Qidiruv..."
                    aria-describedby="basic-addon-search31"
                    />
                </div>
            </div>
            <div class="col-md-3 col-lg-2 mb-3">
                <select class="form-select" id="inputGroupSelect01" name="category_id">
                    <option value="">Kategoriyani tanlang...</option>
                    @foreach($categories as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2 col-lg-2">
                <button type="submit" class="form-control btn btn-info btn-md">Izlash</button>
            </div>
        </div>
    </form>
        <div class="row mb-5">
            @foreach ($products as $item)
                <div class="col-md-6 col-lg-3 mb-3">
                    <div class="card">
                        <div style="width: 100%; height: 200px; display: flex; align-items: center; justify-content: center ">
                            <img class="card-img-top" src="{{ $item->image }}" alt="Card image cap" style="width: 190px; height: 190px; text-align:center;" />
                        </div>
                        <div class="card-body">
                            <a href="{{ route('admin.offer.show', ['offer_id'=> $item->id]) }}"><h5 class="card-title text-center">{{ $item->name }}</h5> </a>
                            <div class="d-flex justify-content-between">
                                <div>Mahsulot narxi: </div>
                                <div>{{ number_format($item->price, 0, ' ', ' ') }} so'm</div>
                            </div>
                            <div class="d-flex justify-content-between" style="margin-top: 10px">
                                <div>Foyda: </div>
                                <div>{{ number_format($item->profit, 0, ' ', ' ' ) }} so'm</div>
                            </div>
                            <div class="d-flex justify-content-between" style="margin-top: 10px">
                              <div>COIN: </div>
                              <div>{{ $item->coin }} </div>
                          </div>

                            <div class="d-flex justify-content-between" style="margin-top: 10px">
                                <button class="form-control btn btn-success"  data-id="{{ $item->id }}" data-bs-toggle="modal" data-bs-target="#createStream"  id="createStreamButton">Oqim yaratish </button>
                            </div>

                            <div class="d-flex justify-content-between" style="margin-top: 10px">
                                <a href="{{  $item->advertising_link}}" target="_blank" class="form-control btn btn-info">Postni ko'rish </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="modal fade" id="createStream" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered1 modal-simple modal-add-new-cc">
            <div class="modal-content">
                <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="position: absolute; top: 1rem; right: 1rem"></button>
                <div class="text-center mb-4">
                    <h3>Oqim yaratish </h3>
                    <p>Oqimga nom bering (kanal nomi yoki ixtiyoriy nom)</p>
                </div>

                    <div class="col-12 col-md-12">
                    <label class="form-label" for="streamName">Oqim nomi</label>
                    <input type="text" id="streamName" class="form-control" placeholder="Savdo tovarlari" />
                    </div>
                <input type="number" hidden id="productID">
                    <div class="col-12 text-center">
                    <button type="button" class="btn btn-primary me-sm-3 me-1 mt-3" onclick="createStream()">Saqlash</button>
                    <button type="reset" class="btn btn-label-secondary btn-reset mt-3" data-bs-dismiss="modal" aria-label="Close">Bekor qilish</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="streamInfo" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered1 modal-simple modal-add-new-cc">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="position: absolute; top: 1rem; right: 1rem"></button>
                <div class="text-center mb-4">
                    <h3>Oqim yaratildi </h3>
                    <p>Quyidagi maxsus link orqali soting va daromad oling</p>
                </div>

                <div class="col-12 col-md-12">
                    <label class="form-label" for="streamName">Oqim uchun maxsus link</label>
                    <p class="streamLink"></p>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Recipient's username" id="streamLink" aria-describedby="basic-addon13" disabled>
                        <span class="input-group-text" id="basic-addon13"><a href="" id="streamLinkHref" target="_blank">  <i class="menu-icon tf-icons bx bx-send"></i> </a> </span>
                    </div>
                </div>
                <div class="col-12 col-md-12">
                    <button type="button" class="btn btn-success  mt-3" onclick="copyStreamLink()">  <i class="menu-icon tf-icons bx bx-copy"></i> Nusxa ko'chirish</button>
                </div>

                <input type="number" hidden id="productID">
                <div class="col-12 col-md-12 text-center">
                <button type="reset" class="btn btn-primary mt-3" data-bs-dismiss="modal" aria-label="Close">Yopish</button>
                </div>
            </div>
        </div>
        </div>
    </div>
    {{ $products->appends(request()->query())->links() }}
</div>
@endsection

<script src="{{ asset('themes/admin/assets/vendor/libs/jquery/jquery.js') }}"></script>

<script>

    $(document).on("click", "#createStreamButton", function () {
        var productID = $(this).data('id');

        $("#productID").val(productID);
        // $('#streamInfo').modal('show');
        // As pointed out in comments,
        // it is unnecessary to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });
    function createStream(){

        var name = document.getElementById("streamName").value
        var productID = document.getElementById("productID").value

        $.ajax('/admin/stream', {
        type: 'POST',  // http method
        data: {
            name: name,
            product_id: productID,
            _token: "{{ csrf_token() }}"
        },  // data to submit
        success: function (data, status, xhr) {
           console.log(data);
           $('#createStream').modal('hide');

           $('#streamLink').val(data.link)
           $("#streamLinkHref").attr("href", data.link)
           $('#streamInfo').modal('show');

        },
        error: function (jqXhr, textStatus, errorMessage) {
                $('p').append('Error' + errorMessage);
        }
    });
    }

    function copyStreamLink() {
      // Get the text field
        var copyText = document.getElementById("streamLink");

        // Select the text field
        copyText.select();
        copyText.setSelectionRange(0, 99999); // For mobile devices

        // Copy the text inside the text field
        navigator.clipboard.writeText(copyText.value);

        // Alert the copied text
        alert("Nusxa ko'chirildi!");
    }

</script>
