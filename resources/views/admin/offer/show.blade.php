@extends('layouts.admin')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card p-4">
        <div class="row">
            <div class="col-lg-6 text-center">
                <img src="{{ $offer->image }}" style="width: 350px; height: 350px;">
            </div>
            <div class="col-lg-6">
                <div style="width: 90%">
                    <h4 class="title">{{ $offer->name }}</h4>
                    <p>ID: {{ $offer->id }}</p>
                    <hr class="line">
                    <div>
                        <span class="price-amount" style="margin-bottom: 0; padding-left: 0;">Mahsulot narxi: <b style="padding-left: 10px"> {{ number_format($offer->price, 0, ' ', ' ') }} so'm</b></span>
                    </div>
                    <div class="pt-3">
                        <span class="price-amount" style="margin-bottom: 0; padding-left: 0;">Foyda: <b style="padding-left: 10px"> {{ number_format($offer->profit, 0, ' ', ' ') }} so'm</b></span>
                    </div>
                    <div class="row mt-5">
                        <div class="col-lg-6 d-flex justify-content-between" style="margin-top: 10px">
                            <button class="form-control btn btn-success"  data-id="{{ $offer->id }}" data-bs-toggle="modal" data-bs-target="#createStream"  id="createStreamButton">Oqim yaratish </button>
                        </div>
                        <div class="col-lg-6 d-flex justify-content-between" style="margin-top: 10px">
                            <a href="{{ $offer->advertising_link }}" target="_blank" class="form-control btn btn-info">Postni ko'rish </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createStream" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered1 modal-simple modal-add-new-cc">
            <div class="modal-content">
                <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="position: absolute; top: 1rem; right: 1rem"></button>
                <div class="text-center mb-4">
                    <h3>Oqim yaratish </h3>
                    <p>Oqimga nom bering (kanal nomi yoki ixtiyoriy nom)</p>
                </div>

                    <div class="col-12 col-md-12">
                    <label class="form-label" for="streamName">Oqim nomi</label>
                    <input type="text" id="streamName" class="form-control" placeholder="Savdo tovarlari" />
                    </div>
                <input type="number" hidden id="productID">
                    <div class="col-12 text-center">
                    <button type="button" class="btn btn-primary me-sm-3 me-1 mt-3" onclick="createStream()">Saqlash</button>
                    <button type="reset" class="btn btn-label-secondary btn-reset mt-3" data-bs-dismiss="modal" aria-label="Close">Bekor qilish</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="streamInfo" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered1 modal-simple modal-add-new-cc">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="position: absolute; top: 1rem; right: 1rem"></button>
                    <div class="text-center mb-4">
                        <h3>Oqim yaratildi </h3>
                        <p>Quyidagi maxsus link orqali soting va daromad oling</p>
                    </div>

                    <div class="col-12 col-md-12">
                        <label class="form-label" for="streamName">Oqim uchun maxsus link</label>
                        <p class="streamLink"></p>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Recipient's username" id="streamLink" aria-describedby="basic-addon13" disabled>
                            <span class="input-group-text" id="basic-addon13"><a href="" id="streamLinkHref" target="_blank">  <i class="menu-icon tf-icons bx bx-send"></i> </a> </span>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">
                        <button type="button" class="btn btn-success  mt-3" onclick="copyStreamLink()">  <i class="menu-icon tf-icons bx bx-copy"></i> Nusxa ko'chirish</button>
                    </div>

                    <input type="number" hidden id="productID">
                    <div class="col-12 col-md-12 text-center">
                    <button type="reset" class="btn btn-primary mt-3" data-bs-dismiss="modal" aria-label="Close">Yopish</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection


<script src="{{ asset('themes/admin/assets/vendor/libs/jquery/jquery.js') }}"></script>

<script>

    $(document).on("click", "#createStreamButton", function () {
        var productID = $(this).data('id');

        $("#productID").val(productID);
        // $('#streamInfo').modal('show');
        // As pointed out in comments,
        // it is unnecessary to have to manually call the modal.
        // $('#addBookDialog').modal('show');
    });
    function createStream(){

        var name = document.getElementById("streamName").value
        var productID = document.getElementById("productID").value

        $.ajax('/admin/stream', {
        type: 'POST',  // http method
        data: {
            name: name,
            product_id: productID,
            _token: "{{ csrf_token() }}"
        },  // data to submit
        success: function (data, status, xhr) {
           console.log(data);
           $('#createStream').modal('hide');

           $('#streamLink').val(data.link)
           $("#streamLinkHref").attr("href", data.link)
           $('#streamInfo').modal('show');

        },
        error: function (jqXhr, textStatus, errorMessage) {
                $('p').append('Error' + errorMessage);
        }
    });
    }

    function copyStreamLink() {
      // Get the text field
        var copyText = document.getElementById("streamLink");

        // Select the text field
        copyText.select();
        copyText.setSelectionRange(0, 99999); // For mobile devices

        // Copy the text inside the text field
        navigator.clipboard.writeText(copyText.value);

        // Alert the copied text
        alert("Nusxa ko'chirildi!");
    }

</script>
