@extends('layouts.admin')

@section('content')

<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-lists-center">
            <h4>Pul yechish uchun so'rovlar ro'yxati</h4>
            @if(auth()->user()->isSeller)
            <h3 class="card-title d-flex">
                <a href="{{ route('admin.money-requests.create') }}" class="btn btn-success" style="float: right"><span class="menu-icon tf-icons bx bx-plus"></span>Yangi so'rov yuborish
                </a>
            </h3>
            @endif
        </div>
    
        <div class="card-body">
            <div class="">
                <table cellpadding="0" cellspacing="0" id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>So'rov ID si</th>
                            <th>Admin ismi</th>
                            <th>Admin raqami</th>
                            <th>Karta raqami</th>
                            <th>So'ralgan summa</th>
                            <th>Yuborilgan vaqti</th>
                            <th>Holati</th>
                            @if(auth()->user()->isAdmin)
                                <th></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i = 1;?>
                        @foreach($requests as $item)
                       
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>#{{ $item->id }} </td>
                            <td>{{ $item->seller->fullname }} </td>
                            <td>{{ $item->seller->phone }} </td>
                            <td>  {{ $item->card_number }} </td>
                            <td>{{ number_format($item->amount, 0, ' ', ' ') }} </td>
                            <td>{{ $item->created_at }} </td>
                            <td><span class="badge bg-label-{{ $statusColors[$item->status] }} me-1">{{ $item->statusLabel }} </span></td>
                            @if(auth()->user()->isAdmin && $item->status == 1)
                            <td>
                               <a href="{{ route('admin.money-requests.change-status', ['request_id'=> $item->id, 'status' => 3]) }}" class="btn btn-success btn-sm">
                                    <i class="menu-icon tf-icons bx bx-check-circle"></i>  <span class="">Tasdiqlash</span>
                                </a>
                                <br>
                                <a href="{{  route('admin.money-requests.change-status', ['request_id'=> $item->id, 'status' => 2]) }}" class="btn btn-danger btn-sm">
                                    <i class="menu-icon tf-icons bx bx-error-alt"></i>  <span class="">Rad etish</span>
                                </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- {{ $products->links() }} --}}
</div>
@endsection