@extends('layouts.admin')

@section('title', 'Moeny Requests')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header">
            <h4>Pul yechish uchun so'rov yuborish</h4>
        </div>
        <form action="{{ route('admin.money-requests.store') }}" method="POST"  class="card-body">
            @csrf()
                <div class="mb-3">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Karta raqami</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="card_number" id="basic-default-name"  @error('card_number') is-invalid @else is-valid @enderror" placeholder="Pul tushirmoqchi bo'lgan plastik karta raqami" />
                    </div>
                </div>
                <div class="mb-3">
                    <label for="amount" class="form-label">So'rov summasi (kamida 50 000 so'm)</label>
                    <input class="form-control @error('amount') is-invalid @else is-valid @enderror" type="number" min="50000"  name="amount" id="amount">
                </div>
                <span class="text-danger">{{ $errors->first('amount') }}</span>

            <button type="submit" class="btn btn-primary btn-md" style="float: right">
                <i class="menu-icon tf-icons bx bx-check"></i>
                {{ __('Ma\'lumotni saqlash') }}
            </button>
        </form>
    </div>
    {{-- {{ $categories->links() }} --}}
</div>
@stop
@section('js')
@stop
