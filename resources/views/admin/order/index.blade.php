@extends('layouts.admin')

@section('content')

<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-lists-center">
            <h4>Buyurtmalar ro'yxati</h4>
        </div>
        <div class="card-body">
            <div class="">
                <table cellpadding="0" cellspacing="0" id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Buyurtma ID si</th>
                            <th>Mahsulot</th>
                            <th>Mahsulot narxi</th>
                            @if (auth()->user()->isSeller)
                                <th>Profit</th>
                            @endif
                            <th>Mijoz ismi</th>
                            <th>Mijoz raqami</th>
                            <th>Buyurtma vaqti</th>
                            <th>Holati</th>
                            @if (!auth()->user()->isSeller)
                            <th></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i = 1;?>
                        @foreach($orders as $item)
                       
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>#{{ $item->id }} </td>
                            <td>  <a href="{{ route('admin.orders.show', ['order_id'=> $item->id]) }}"> {{ $item->product->name }} </a></td>
                            <td>{{ number_format($item->product->price, 0, ' ', ' ') }} </td>
                            @if (auth()->user()->isSeller)
                                <td>{{ number_format($item->product->profit, 0, ' ', ' ') }}</td>
                            @endif
                            <td>{{ $item->customer_name }} </td>
                            <td>{{ $item->phone_number }} </td>
                            <td>{{ $item->created_at }} </td>
                            <td><span class="badge bg-label-{{ $statusColors[$item->status] }} me-1">{{ $item->statusLabel }} </span></td>
                            @if (!auth()->user()->isSeller)
                            <td class="d-flex flex-wrap">
                                <a href="{{ route('admin.orders.show', ['order_id'=> $item->id]) }}" class="btn btn-success btn-sm">
                                    <i class="menu-icon tf-icons bx bx-edit-alt"></i>  <span class="">Tahrirlash</span>
                                </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- {{ $products->links() }} --}}
</div>
@endsection