@extends('layouts.admin')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-xl-9 col-md-8 col-12 mb-md-0 mb-4">
            <div class="card">
                <div class="card-body">    
                    Buyurtma haqida ma'lumot
                    <hr>
                    <table class="table table-bordered">
                        <tr>
                            <td>Mahsulot nomi:</td>
                            <td>{{ $order->product->name }}</td>
                        </tr>
                        
                        <tr>
                            <td>Mahsulot narxi:</td>
                            <td>{{ number_format($order->product->price,0, ' ', ' ')  }}</td>
                        </tr>
                        
                        <tr>
                            <td>Mijoz ismi:</td>
                            <td>{{ $order->customer_name}}</td>
                        </tr>
                        
                        <tr>
                            <td>Telefon raqam:</td>
                            <td>{{ $order->phone_number }}</td>
                        </tr>
                        <tr>
                            <td>Hudud:</td>
                            <td>{{ $order->region ? $order->region->name : '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        @if(!auth()->user()->isSeller)
        <div class="col-xl-3 col-md-4 col-12">
            <div class="card">
                <div class="card-body">
                    Buyurtma holatini o'zgartirish
                    <hr>
                    <form action="{{ route('admin.orders.update', ['order_id' => $order->id]) }}" method="post">
                        @csrf

                    <label for="exampleDataList" class="form-label">Holati</label>
                    <select class="form-select mb-3" id="inputGroupSelect01" name="status">
                        @foreach ($statuses as $key => $status)
                            @if($key == $order->status)
                             <option value="{{ $key }}" selected>{{ $status }} </option>
                             @else
                             <option value="{{ $key }}">{{ $status }} </option>
                            @endif
                        @endforeach
                    </select>

                  <button class="btn btn-primary d-grid w-100" type="submit" data-bs-toggle="offcanvas" data-bs-target="#addPaymentOffcanvas">
                    <span class="d-flex align-items-center justify-content-center text-nowrap"><i class="bx bx-send bx-xs me-1"></i>O'zgartirish</span>
                  </button>
                </form>
                </div>
              </div>
        </div>
        @endif
    </div>
</div>
    
@endsection