@extends('layouts.admin')

@section('title', 'Category')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header">
            <h4>Kategoriya yaratish</h4>
        </div>
        <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data" class="card-body">
            @csrf()
                <div class="mb-3">
                    <label class="col-sm-3 col-form-label" for="basic-default-name">Kategoriya nomi</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="name" id="basic-default-name"  @error('name') is-invalid @else is-valid @enderror" placeholder="Kategoriya nomi" />
                    </div>
                </div>
                <div class="mb-3">
                    <label for="formFileMultiple" class="form-label">Kategoriya rasmi</label>
                    <input class="form-control @error('image') is-invalid @else is-valid @enderror" type="file" name="image" id="image">
                </div>
                <span class="text-danger">{{ $errors->first('image') }}</span>

            <button type="submit" class="btn btn-primary btn-md btn-xl btn-sm" style="float: right">
                <i class="menu-icon tf-icons bx bx-check"></i>
                {{ __('Ma\'lumotni saqlash') }}
            </button>
        </form>
    </div>
    {{-- {{ $categories->links() }} --}}
</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

</script>
@stop
