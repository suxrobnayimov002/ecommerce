@extends('layouts.admin')

@section('title', 'Category')

@section('content_header')
<h4>Category</h4>
@stop

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h4>Kategoriyalar ro'yxati</h4>
            <h3 class="card-title d-flex">
                <a href="{{ route('categories.create') }}" class="btn btn-primary" style="float: right"><span class="menu-icon tf-icons bx bx-plus"></span>Ma'lumot qo'shish
                </a>
            </h3>
        </div>



        <div class="card-body">
            <div class="">
                <table cellpadding="0" cellspacing="0" id="example" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Kategoriya nomi</th>
                            <th>Kategoriya rasmi</th>
                            <th>Amallar</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i = 1;?>
                        @foreach($categories as $item)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $item->name }} </td>
                            <td><img src="{{ $item->image }}" style="width:120px; height: 80px"></td>
                            <td class="d-flex">
                                <a href="{{ route('categories.edit',  $item->id) }}" class="btn btn-info btn-sm" style="margin-right: 20px">
                                    <i class="menu-icon tf-icons bx bx-edit-alt"></i>
                                    <span class="">Tahrirlash</span>
                                </a>
                                <form action="{{ route('categories.destroy',  $item->id) }}" method="post" onsubmit="return confirm('Сиз ростдан ҳам ушбу маълумотни ўчиришни хохлайсизми ?')">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-sm btn-danger">
                                        <i class="menu-icon tf-icons bx bx-trash"></i>
                                        <span class="">O'chirish</span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $categories->links() }}
</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

</script>
@stop
