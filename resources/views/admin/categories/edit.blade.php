@extends('layouts.admin')

@section('title', 'Category')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <div class="card">
        <div class="card-header">
            <h4>Kategoriyani tahrirlash</h4>
        </div>
        <form action="{{ route('categories.update', $item[0]->id) }}" method="POST" enctype="multipart/form-data" class="card-body">
            @csrf()
            @method('PUT')
                <div class="row mb-3">
                    <label class="col-sm-1 col-form-label" for="basic-default-name">Kategoriya nomi</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="name" value="{{ $item[0]->name }}" id="basic-default-name"  @error('name') is-invalid @else is-valid @enderror" placeholder="Category Name" />
                    </div>
                </div>
                <div class="mb-3">
                    <label for="formFileMultiple" class="form-label">Kategoriya rasmi</label>
                    <input class="form-control" type="file" name="image" value="{{ $item[0]->image }}" id="formFileMultiple" multiple @error('image') is-invalid @else is-valid @enderror" />
                </div>
                <span class="text-danger">{{ $errors->first('name') }}</span>

            <button type="submit" class="btn btn-primary btn-md" style="float: right !important;">
                <i class="menu-icon tf-icons bx bx-check"></i>
                {{ __('Ma\'lumotni saqlash') }}
            </button>
        </form>
    </div>
</div>
@stop
@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

</script>
@stop
