<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>oSavdo || Kirish </title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('themes/shop/assets/images/favicon.png') }}">

    <!-- CSS
    ============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/flaticon/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/sal.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/base.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/style.min.css') }}">

</head>


<body>
    <div class="axil-signin-area">

        <!-- Start Header -->
        <div class="signin-header">
            <div class="row align-items-center">
                <div class="col-sm-4">
                    <a href="/" class="site-logo"><img src="{{ asset('themes/images/logol.jpg') }}" alt="logo"></a>
                </div>
                <div class="col-sm-8">
                    <div class="singin-header-btn">
                        <p>Ro'yxatdan o'tmaganmisiz?</p>
                        <a href="{{ route('register') }}" class="axil-btn btn-bg-secondary sign-up-btn">Ro'yxatdan o'ting</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header -->

        <div class="row">
            <div class="col-xl-4 col-lg-6">
                <div class="axil-signin-banner bg_image bg_image--9">
                    <h3 class="title">Biz faqat eng yaxshilarini tavsiya qilamiz!</h3>
                </div>
            </div>
            <div class="col-lg-6 offset-xl-2">
                <div class="axil-signin-form-wrap">
                    <div class="axil-signin-form">
                        <h3 class="title">oSavdo tizimiga kirish</h3>
                        <p class="b2 mb--55">Telefon va parolingizni kiriting</p>
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <form class="singin-form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="username" :value="__('Phone')">Telefon raqam</label>
                                <input type="text" class="form-control" name="username" :value="old('username')" required>
                            </div>
                            <div class="form-group">
                                <label for="password" :value="__('Password')">Parol</label>
                                <input type="password" class="form-control" name="password" :value="old('password')" required">
                            </div>
                            <div class="form-group align-items-center justify-content-between">
                                <button type="submit" class="axil-btn btn-bg-primary submit-btn">Kirish</button>
                                {{-- <a href="" class="forgot-btn">Forget password?</a> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JS
============================================ -->
    <!-- Modernizer JS -->
    <script src="{{ ('themes/shop/assets/js/vendor/modernizr.min.js') }}"></script>
    <!-- jQuery JS -->
    <script src="{{ ('themes/shop/assets/js/vendor/jquery.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ ('themes/shop/assets/js/vendor/popper.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/slick.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/js.cookie.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/jquery-ui.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/jquery.countdown.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/sal.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/isotope.pkgd.min.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/counterup.js') }}"></script>
    <script src="{{ ('themes/shop/assets/js/vendor/waypoints.min.js') }}"></script>

    <!-- Main JS -->
    <script src="{{ ('themes/shop/assets/js/main.js') }}"></script>

</body>

</html>
{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}


