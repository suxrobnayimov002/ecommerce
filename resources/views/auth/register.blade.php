<!doctype html>
<html class="no-js') }}" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>oSavdo || Ro'yxatdan o'tish</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('themes/shop/assets/images/favicon.png') }}">

    <!-- CSS
    ============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/flaticon/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/sal.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/base.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/style.min.css') }}">

</head>


<body>
    <div class="axil-signin-area">

        <!-- Start Header -->
        <div class="signin-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <a href="/" class="site-logo"><img src="{{ asset('themes/images/logol.jpg') }}" alt="logo"></a>
                </div>
                <div class="col-md-6">
                    <div class="singin-header-btn">
                        <p>Avval ro'yxatdan o'tganmisiz?</p>
                        <a href="{{ route('login') }}" class="axil-btn btn-bg-secondary sign-up-btn">Tizimga kiring</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header -->

        <div class="row">
            <div class="col-xl-4 col-lg-6">
                <div class="axil-signin-banner bg_image bg_image--10">
                    <h3 class="title">Biz faqat eng yaxshilarini tavsiya qilamiz!</h3>
                </div>
            </div>
            <div class="col-lg-6 offset-xl-2">
                <div class="axil-signin-form-wrap">
                    <div class="axil-signin-form">
                        <h3 class="title">Ro'yxatdan o'tish</h3>
                        <br>
                        {{-- <p class="b2 mb--55">Ma'lumotlaringizni kiriting</p> --}}
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <form class="singin-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <label for="name" :value="__('Name')">Ismingiz</label>
                                <input type="text" class="form-control" name="name"  :value="old('name')" required>
                            </div>
                            <div class="form-group">
                                <label for="phone" :value="__('Phone')">Telefon raqamingiz</label>
                                <input type="number" class="form-control" name="phone" id="phone":value="old('phone')" required>
                            </div>
                            <div class="form-group">
                                <label for="password" :value="__('Password')">Parol</label>
                                <input type="password" class="form-control" name="password" required autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <label for="confirm_password" :value="__('ConfirmPassword')">Parolni qayta kiriting</label>
                                <input type="password" class="form-control" name="password_confirmation" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="axil-btn btn-bg-primary submit-btn">Ro'yxatdan o'tish</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JS
============================================ -->
    <!-- Modernizer JS -->
    <script src="{{ asset('themes/shop/assets/js/vendor/modernizr.min.js') }}"></script>
    <!-- jQuery JS -->
    <script src="{{ asset('themes/shop/assets/js/vendor/jquery.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('themes/shop/assets/js/vendor/popper.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/js.cookie.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/sal.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/counterup.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/waypoints.min.js') }}"></script>

    <!-- Main JS -->
    <script src="{{ asset('themes/shop/assets/js/main.js') }}"></script>

</body>

</html>

{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}
