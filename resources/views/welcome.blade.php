<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>oSavdo</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('themes/shop/assets/images/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/flaticon/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/sal.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/vendor/base.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/shop/assets/css/style.min.css') }}">
</head>
<body class="sticky-header newsletter-popup-modal">
    <a href="#top" class="back-to-top" id="backto-top"><i class="fal fa-arrow-up"></i></a>

    @include('layouts.includes.header')

    <main class="main-wrapper">

        {{-- Slider --}}
        <div class="axil-main-slider-area main-slider-style-5">
            <div class="container">
                <div class="slider-box-wrap">
                    <div class="slider-activation-two axil-slick-dots">
                        @foreach ($last_products as $list )
                            <div class="single-slide slick-slide">
                                <div class="main-slider-content">
                                    <span class="subtitle"><i class="fas fa-fire"></i> Kun mahsuloti</span>
                                    <h1 class="title">{{ $list->name }}</h1>
                                    <div class="shop-btn">
                                        <a href="{{ route('product.show',  $list->id) }}" class="axil-btn btn-bg-white"><i class="fal fa-shopping-cart"></i> Hozir xarid qiling</a>
                                    </div>
                                </div>
                                <div class="main-slider-thumb">
                                    <img src="{{ $list->image }}" alt="Product">
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        {{-- Categories --}}
        <div class="axil-categorie-area bg-color-white axil-section-gapcommon">
            <div class="container">
                <div class="section-title-wrapper">
                    <span class="title-highlighter highlighter-secondary"> <i class="far fa-tags"></i>Kategoriyalar</span>
                    <h2 class="title">Barcha kategoriyalar</h2>
                </div>
                <div class="categrie-product-activation slick-layout-wrapper--15 axil-slick-arrow  arrow-top-slide">
                    @foreach ($categories as $item )
                        <div class="slick-single-layout">
                            <div class="categrie-product" data-sal="zoom-out" data-sal-delay="200" data-sal-duration="500">
                                <a href="{{ route('product.index', $item->category_id ) }}">
                                    <img class="img-fluid" src="{{ $item->image }}" style="width: 64px; height: 64px" alt="product categorie">
                                    <h6 class="cat-title">{{ $item->name }}</h6>
                                    <p class="cat-title">Mahsulotlar : {{ $item->products_count }}</p>
                                </a>
                            </div>
                            <!-- End .categrie-product -->
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        {{-- Products --}}
        @include('layouts.includes.product', ['products' => $products])

        {{-- New arrivals --}}
        <div class="axil-new-arrivals-product-area bg-color-white axil-section-gap pb--0">
            <div class="container">
                <div class="product-area pb--50">
                    <div class="section-title-wrapper section-title-center">
                        <span class="title-highlighter highlighter-primary"><i class="far fa-shopping-basket"></i>Hafta bo'yicha</span>
                        <h2 class="title">Yangi mahsulotlar</h2>
                    </div>
                    <div class="new-arrivals-product-activation slick-layout-wrapper--30 axil-slick-arrow  arrow-top-slide">
                        @foreach ($last_products as $item )
                            <div class="slick-single-layout">
                                <div class="axil-product product-style-two">
                                    <div class="thumbnail">
                                        <a href="{{ route('product.show',  $item->id) }}">
                                            <img data-sal="zoom-out" data-sal-delay="200" data-sal-duration="500" src="{{ $item->image }} " alt="Product Images">
                                        </a>
                                    </div>
                                    <div class="product-content">
                                        <div class="inner">
                                            <h5 class="title"><a href="{{ route('product.show',  $item->id) }}">{{ $item->name }}</a></h5>
                                            <div class="product-price-variant">
                                                <span class="price current-price">{{ number_format($item->price, 0, ' ', ' ' ) }} so'm</span>
                                            </div>
                                            <div class="product-hover-action">
                                                <ul class="cart-action">
                                                    <li class="quickview"><a href="#" data-bs-toggle="modal" data-bs-target="#quick-view-modal"><i class="far fa-eye"></i></a></li>
                                                    <li class="select-option"><a href="{{ route('product.show',  $item->id) }}">Buyurtma berish</a></li>
                                                    <li class="wishlist"><a href="prevent()"  data-id="{{  $item->id }}" id="addToWishlist"><i class="far fa-heart"></i></a></li>
                                                </ul>
                                                {{-- <div class="alert alert-success" role="alert">
                                                    This is a success alert—check it out!
                                                  </div> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        {{-- Most sold --}}
        <div class="axil-most-sold-product axil-section-gap">
            <div class="container">
                <div class="product-area pb--50">
                    <div class="section-title-wrapper section-title-center">
                        <span class="title-highlighter highlighter-primary"><i class="fas fa-star"></i> Eng ko'p sotilganlar</span>
                        <h2 class="title">Eng ko'p sotilganlar</h2>
                    </div>
                    <div class="row row-cols-xl-2 row-cols-1 row--15">
                        @foreach ($products as $product)
                        <div class="col">
                            <div class="axil-product-list">
                                <div class="thumbnail">
                                    <a href="single-product.html">
                                        <img data-sal="zoom-in" data-sal-delay="100" data-sal-duration="1500" src="{{ $product->image }}"  style="width:120px; height:120px"    alt="Yantiti Leather Bags">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <div class="product-rating">
                                        <span class="rating-icon">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fal fa-star"></i>
                                </span>
                                        <span class="rating-number"><span>100+</span> Reviews</span>
                                    </div>
                                    <h6 class="product-title"><a href="{{ route('product.show',  $item->id) }}"> {{ $product->name }}</a></h6>
                                    <div class="product-price-variant">
                                        <span class="price current-price">{{ number_format($product->price, 0, ' ', ' '  ) }}</span>
                                        {{-- <span class="price old-price">$49.99</span> --}}
                                    </div>
                                    <div class="product-cart">
                                        <a href="{{ route('product.show',  $item->id) }}" class="cart-btn"><i class="fal fa-shopping-cart"></i></a>
                                        <a href="wishlist.html" class="cart-btn"><i class="fal fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                       
                     
                    </div>
                </div>
            </div>
        </div>

        {{-- Why people choose us --}}
        <div class="axil-why-choose-area pb--50 pb_sm--30">
            <div class="container">
                <div class="section-title-wrapper section-title-center">
                    <span class="title-highlighter highlighter-secondary"><i class="fal fa-thumbs-up"></i>Nima uchun biz</span>
                    <h2 class="title">Nega mijozlar bizni tanlashadi</h2>
                </div>
                <div class="row row-cols-xl-5 row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-1 row--20">
                    <div class="col">
                        <div class="service-box">
                            <div class="icon">
                                <img src="{{ asset('themes/shop/assets/images/icons/service6.png') }}" alt="Service">
                            </div>
                            <h6 class="title">Tezkor &amp; Xavfsiz yetkazish</h6>
                        </div>
                    </div>
                    <div class="col">
                        <div class="service-box">
                            <div class="icon">
                                <img src="{{ asset('themes/shop/assets/images/icons/service7.png') }}" alt="Service">
                            </div>
                            <h6 class="title">Mahsulotlarga 100% kafolat</h6>
                        </div>
                    </div>
                    <div class="col">
                        <div class="service-box">
                            <div class="icon">
                                <img src="{{ asset('themes/shop/assets/images/icons/service8.png') }}" alt="Service">
                            </div>
                            <h6 class="title">Yuqori sifatdagi <br> mahsulotlar</h6>
                        </div>
                    </div>
                    <div class="col">
                        <div class="service-box">
                            <div class="icon">
                                <img src="{{ asset('themes/shop/assets/images/icons/service9.png') }}" alt="Service">
                            </div>
                            <h6 class="title">Toʻlovni mahsulot olganda qilish</h6>
                        </div>
                    </div>
                    <div class="col">
                        <div class="service-box">
                            <div class="icon">
                                <img src="{{ asset('themes/shop/assets/images/icons/service10.png') }}" alt="Service">
                            </div>
                            <h6 class="title">Qoʻllab quvvatlash xizmati</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Start Footer Area  -->
    @include('layouts.includes.footer')
    <!-- End Footer Area  -->

    <!-- Product Quick View Modal Start -->
    <div class="modal fade quick-view-product" id="quick-view-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="far fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <div class="single-product-thumb">
                        <div class="row">
                            <div class="col-lg-7 mb--40">
                                <div class="row">
                                    <div class="col-lg-10 order-lg-2">
                                        <div class="single-product-thumbnail product-large-thumbnail axil-product thumbnail-badge zoom-gallery">
                                            <div class="thumbnail">
                                                <img src="{{ asset('themes/shop/assets/images/product/product-big-01.png') }}" alt="Product Images">
                                                <div class="label-block label-right">
                                                    <div class="product-badget">20% OFF</div>
                                                </div>
                                                <div class="product-quick-view position-view">
                                                    <a href="{{ asset('themes/shop/assets/images/product/product-big-01.png') }}" class="popup-zoom">
                                                        <i class="far fa-search-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="thumbnail">
                                                <img src="{{ asset('themes/shop/assets/images/product/product-big-02.png') }}" alt="Product Images">
                                                <div class="label-block label-right">
                                                    <div class="product-badget">20% OFF</div>
                                                </div>
                                                <div class="product-quick-view position-view">
                                                    <a href="{{ asset('themes/shop/assets/images/product/product-big-02.png') }}" class="popup-zoom">
                                                        <i class="far fa-search-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="thumbnail">
                                                <img src="{{ asset('themes/shop/assets/images/product/product-big-03.png') }}" alt="Product Images">
                                                <div class="label-block label-right">
                                                    <div class="product-badget">20% OFF</div>
                                                </div>
                                                <div class="product-quick-view position-view">
                                                    <a href="{{ asset('themes/shop/assets/images/product/product-big-03.png') }}" class="popup-zoom">
                                                        <i class="far fa-search-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 order-lg-1">
                                        <div class="product-small-thumb small-thumb-wrapper">
                                            <div class="small-thumb-img">
                                                <img src="{{ asset('themes/shop/assets/images/product/product-thumb/thumb-08.png') }}" alt="thumb image">
                                            </div>
                                            <div class="small-thumb-img">
                                                <img src="{{ asset('themes/shop/assets/images/product/product-thumb/thumb-07.png') }}" alt="thumb image">
                                            </div>
                                            <div class="small-thumb-img">
                                                <img src="{{ asset('themes/shop/assets/images/product/product-thumb/thumb-09.png') }}" alt="thumb image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 mb--40">
                                <div class="single-product-content">
                                    <div class="inner">
                                        <div class="product-rating">
                                            <div class="star-rating">
                                                <img src="{{ asset('themes/shop/assets/images/icons/rate.png') }}" alt="Rate Images">
                                            </div>
                                            <div class="review-link">
                                                <a href="#">(<span>1</span> customer reviews)</a>
                                            </div>
                                        </div>
                                        <h3 class="product-title">Serif Coffee Table</h3>
                                        <span class="price-amount">$155.00 - $255.00</span>
                                        <ul class="product-meta">
                                            <li><i class="fal fa-check"></i>In stock</li>
                                            <li><i class="fal fa-check"></i>Free delivery available</li>
                                            <li><i class="fal fa-check"></i>Sales 30% Off Use Code: MOTIVE30</li>
                                        </ul>
                                        <p class="description">In ornare lorem ut est dapibus, ut tincidunt nisi pretium. Integer ante est, elementum eget magna. Pellentesque sagittis dictum libero, eu dignissim tellus.</p>

                                        <div class="product-variations-wrapper">

                                            <!-- Start Product Variation  -->
                                            <div class="product-variation">
                                                <h6 class="title">Colors:</h6>
                                                <div class="color-variant-wrapper">
                                                    <ul class="color-variant mt--0">
                                                        <li class="color-extra-01 active"><span><span class="color"></span></span>
                                                        </li>
                                                        <li class="color-extra-02"><span><span class="color"></span></span>
                                                        </li>
                                                        <li class="color-extra-03"><span><span class="color"></span></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- End Product Variation  -->

                                            <!-- Start Product Variation  -->
                                            <div class="product-variation">
                                                <h6 class="title">Size:</h6>
                                                <ul class="range-variant">
                                                    <li>xs</li>
                                                    <li>s</li>
                                                    <li>m</li>
                                                    <li>l</li>
                                                    <li>xl</li>
                                                </ul>
                                            </div>
                                            <!-- End Product Variation  -->

                                        </div>

                                        <!-- Start Product Action Wrapper  -->
                                        <div class="product-action-wrapper d-flex-center">
                                            <!-- Start Quentity Action  -->
                                            <div class="pro-qty"><input type="text" value="1"></div>
                                            <!-- End Quentity Action  -->

                                            <!-- Start Product Action  -->
                                            <ul class="product-action d-flex-center mb--0">
                                                <li class="add-to-cart"><a href="cart.html" class="axil-btn btn-bg-primary">Add to Cart</a></li>
                                                <li class="wishlist"><a href="wishlist.html" class="axil-btn wishlist-btn"><i class="far fa-heart"></i></a></li>
                                            </ul>
                                            <!-- End Product Action  -->

                                        </div>
                                        <!-- End Product Action Wrapper  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product Quick View Modal End -->

    <!-- Header Search Modal End -->
    <div class="header-search-modal" id="header-search-modal">
        <button class="card-close sidebar-close"><i class="fas fa-times"></i></button>
        <div class="header-search-wrap">
            <div class="card-header">
                <form action="{{ route('product.index') }}">
                    <div class="input-group">
                        <input type="search" class="form-control" name="product_name" id="prod-search" placeholder="Mahsulot nomi boʻyicha izlang....">
                        <button type="submit" class="axil-btn btn-bg-primary"><i class="far fa-search"></i></button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- Header Search Modal End -->



    <div class="cart-dropdown" id="cart-dropdown">
        <div class="cart-content-wrap">
            <div class="cart-header">
                <h2 class="header-title">Cart review</h2>
                <button class="cart-close sidebar-close"><i class="fas fa-times"></i></button>
            </div>
            <div class="cart-body">
                <ul class="cart-item-list">
                    <li class="cart-item">
                        <div class="item-img">
                            <a href="single-product.html"><img src="{{ asset('themes/shop/assets/images/product/electric/product-01.png') }}" alt="Commodo Blown Lamp"></a>
                            <button class="close-btn"><i class="fas fa-times"></i></button>
                        </div>
                        <div class="item-content">
                            <div class="product-rating">
                                <span class="icon">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</span>
                                <span class="rating-number">(64)</span>
                            </div>
                            <h3 class="item-title"><a href="single-product-3.html">Wireless PS Handler</a></h3>
                            <div class="item-price"><span class="currency-symbol">$</span>155.00</div>
                            <div class="pro-qty item-quantity">
                                <input type="number" class="quantity-input" value="15">
                            </div>
                        </div>
                    </li>
                    <li class="cart-item">
                        <div class="item-img">
                            <a href="single-product-2.html"><img src="{{ asset('themes/shop/assets/images/product/electric/product-02.png') }}" alt="Commodo Blown Lamp"></a>
                            <button class="close-btn"><i class="fas fa-times"></i></button>
                        </div>
                        <div class="item-content">
                            <div class="product-rating">
                                <span class="icon">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</span>
                                <span class="rating-number">(4)</span>
                            </div>
                            <h3 class="item-title"><a href="single-product-2.html">Gradient Light Keyboard</a></h3>
                            <div class="item-price"><span class="currency-symbol">$</span>255.00</div>
                            <div class="pro-qty item-quantity">
                                <input type="number" class="quantity-input" value="5">
                            </div>
                        </div>
                    </li>
                    <li class="cart-item">
                        <div class="item-img">
                            <a href="single-product-3.html"><img src="{{ asset('themes/shop/assets/images/product/electric/product-03.png') }}" alt="Commodo Blown Lamp"></a>
                            <button class="close-btn"><i class="fas fa-times"></i></button>
                        </div>
                        <div class="item-content">
                            <div class="product-rating">
                                <span class="icon">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</span>
                                <span class="rating-number">(6)</span>
                            </div>
                            <h3 class="item-title"><a href="single-product.html">HD CC Camera</a></h3>
                            <div class="item-price"><span class="currency-symbol">$</span>200.00</div>
                            <div class="pro-qty item-quantity">
                                <input type="number" class="quantity-input" value="100">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="cart-footer">
                <h3 class="cart-subtotal">
                    <span class="subtotal-title">Subtotal:</span>
                    <span class="subtotal-amount">$610.00</span>
                </h3>
                <div class="group-btn">
                    <a href="cart.html" class="axil-btn btn-bg-primary viewcart-btn">View Cart</a>
                    <a href="checkout.html" class="axil-btn btn-bg-secondary checkout-btn">Checkout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- JS
        ============================================ -->
        <!-- Modernizer JS -->
        <script src="{{ asset('themes/shop/assets/js/vendor/modernizr.min.js') }}"></script>
        <!-- jQuery JS -->
        <script src="{{ asset('themes/shop/assets/js/vendor/jquery.js') }}"></script>
        <!-- Bootstrap JS -->
        <script src="{{ asset('themes/shop/assets/js/vendor/popper.min.js') }}"></script>
        <script src="{{ asset('themes/shop/assets/js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ asset('themes/shop/assets/js/vendor/slick.min.js') }}"></script>
        <script src="{{ asset('themes/shop/assets/js/vendor/js.cookie.js') }}"></script>
        <script src="{{ asset('themes/shop/assets/js/vendor/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('themes/shop/assets/js/vendor/jquery.countdown.min.js') }}"></script>
        <script src="{{ asset('themes/shop/assets/js/vendor/sal.js') }}"></script>
        <script src="{{ asset('themes/shop/assets/js/vendor/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/counterup.js') }}"></script>
    <script src="{{ asset('themes/shop/assets/js/vendor/waypoints.min.js') }}"></script>

    <!-- Main JS -->
    <script src="{{ asset('themes/shop/assets/js/main.js') }}"></script>

    <script>
          $(document).on("click", "#addToWishlist", function () {

                event.preventDefault();
                var productID = $(this).data('id');
                addWishlist(productID);

            });

          function addWishlist(productID){


            $.ajax('/product-wishlist/'+productID, {
            type: 'POST',  // http method
            data: {
                _token: "{{ csrf_token() }}"
            },  // data to submit
            success: function (data, status, xhr) {
            console.log(data);

            },
            error: function (jqXhr, textStatus, errorMessage) {
                    $('p').append('Error' + errorMessage);
            }
            });
            }
    </script>

</body>

</html>
