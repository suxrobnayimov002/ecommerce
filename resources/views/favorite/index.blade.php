@extends('layouts.product')

@section('content')
<main class="main-wrapper">
    <div class="axil-wishlist-area axil-section-gap">
        <div class="container">
            <div class="product-table-heading">
                <h4 class="title">Sevimli mahsulotlarim</h4>
            </div>
            <div class="table-responsive">
                <table class="table axil-product-table axil-wishlist-table">
                    <thead>
                        <tr>
                            <th scope="col" class="product-remove"></th>
                            <th scope="col" class="product-thumbnail">Mahsulot</th>
                            <th scope="col" class="product-title">Mahsulot nomi</th>
                            <th scope="col" class="product-price">Mahsulot narxi</th>
                            <th scope="col" class="product-add-cart"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $item)

                        <tr>
                            <td class="product-remove"><a href="#" class="remove-wishlist" id="removeFromWishlist"  data-id="{{ $item->id }}"><i class="fal fa-times"></i></a></td>
                            <td class="product-thumbnail"><a href="{{ route('product.show', ['products' => $item->product->id]) }}"><img src="{{ $item->product->image }}"></a></td>
                            <td class="product-title"><a href="{{ route('product.show', ['products' => $item->product->id]) }}">{{ $item->product->name  }}</a></td>
                            <td class="product-price" data-title="Price">{{ number_format($item->product->price,  0 , ' ', ' ') }}</td>
                            <td class="product-add-cart"><a href="{{ route('product.show', ['products' => $item->product->id]) }}" class="axil-btn btn-outline">Buyurtma berish</a></td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@endsection
<script src="{{ asset('themes/shop/assets/js/vendor/jquery.js') }}"></script>

<script>

    $(document).on("click", "#removeFromWishlist", function () {

          event.preventDefault();
          var productID = $(this).data('id');

          removeWishlist(productID);
      });
    function removeWishlist(productID){


      $.ajax('/product-wishlist/'+productID, {
      type: 'DELETE',  // http method
      data: {
          _token: "{{ csrf_token() }}"
      },  // data to submit
      success: function (data, status, xhr) {

        location.reload();

      },
      error: function (jqXhr, textStatus, errorMessage) {
              $('p').append('Error' + errorMessage);
      }
      });
      }
</script>
