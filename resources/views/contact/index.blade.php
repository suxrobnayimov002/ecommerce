@extends('layouts.product')

@section('content')
<div class="axil-wishlist-area axil-section-gap">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-8">
                <div class="inner">
                    <ul class="axil-breadcrumb">
                        <li class="axil-breadcrumb-item"><a href="/">Bosh sahifa</a></li>
                        <li class="separator"></li>
                        <li class="axil-breadcrumb-item active" aria-current="page">Kontaktlar</li>
                    </ul>
                    <h1 class="title">Murojaat uchun</h1>
                </div>
            </div>
        </div>
        <div class="contact">
            <div class="contact-info">
                <p>
                    <b class="title">Call-center:</b>
                    <span class="text">+998 91 123 45 67</span>
                </p>
            </div>
            <div class="contact-info">
                <p>
                    <b class="title">Elektron pochta: </b>
                    <span class="text">info@osavdo.uz</span>
                </p>
            </div>
            <div class="contact-info">
                <p>
                    <b class="title">Telegram: </b>
                    <span class="text"><a href="https://t.me/osavdo_support" target="_blank">@osavdo_support</a></span>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
