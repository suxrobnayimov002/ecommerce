@extends('layouts.admin')

@section('title', 'Stream')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
   <div class="row">
        @foreach ($streams as $stream)
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mb-3">
                <div class="card">
                    <div class="card-header">
                        {{ $stream->name }}
                        <p style="font-size: 12px">{{ $stream->product->name }}</p>
                        {{-- <a href="https://sotuvchi.com/webmaster/stream/stats/76625" class="text-info">~nFqAe~</a> --}}
                    </div>
                    <div style="width: 100%; max-height: 200px; display: flex; align-items: center; justify-content: center ">
                        <img class="card-img-top" src="{{ $stream->product->image }}" alt="Card image cap" style="width: 190px; height: 190px; text-align:center;" />
                    </div>

                    <div class="card-footer">
                        <div class="row">
                            <div class="col-12">
                                <a href="{{ $stream->product->advertising_link }}" class="btn btn-info w-100">
                               
                                    <i class="bx bx-send bx-xs me-1"></i>  Postni ko'rish
                                </a>
                            </div>
                            <br><br>
                            
                            <div class="col-12">
                                <a href="{{ $stream->fullUrl }}" target="_blank" class="btn btn-success w-100"><i class="bx bx-show bx-xs me-1"></i>  Oqimni ko'rish</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
