<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\AuthController as AdminAuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\OfferController;
use App\Http\Controllers\Admin\StreamController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductWishlistController;
use App\Http\Controllers\SellerMoneyRequestController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'home'])->name('layouts.welcome');
Route::get('account', [AccountController::class, 'index'])->name('account.index');
Route::get('contact', [HomeController::class, 'contact'])->name('contact.index');
Route::get('/products/all', [App\Http\Controllers\ProductController::class, 'index'])->name('product.index');
Route::get('/products/{products}',  [ProductController::class,'show'])->name('product.show');

Route::post('order', [OrderController::class, 'store'])->name('order.store');
Route::get('/favorites', [ProductWishlistController::class, 'index'])->name('favorite.index');

Route::get('/s/{uuid}', [StreamController::class, 'show'])->name('stream.show');

Route::middleware('auth')->group(function(){

    Route::get('/product-wishlist', [ProductWishlistController::class,'index'])->name('wishlist.index');
    Route::post('/product-wishlist/{product_id}', [ProductWishlistController::class,'store']);
    Route::delete('/product-wishlist/{product_id}', [ProductWishlistController::class,'destroy']);

    Route::get('/my-orders', [OrderController::class, 'myOrders'])->name('customer.orders');

    // Route::middleware(['role|customer'])->group(function(){
    // });
});

Route::prefix('admin')->group(function () {

    Route::get('/login', [AdminAuthController::class, 'loginShow']);
    Route::post('/login', [AdminAuthController::class, 'login'])->name('admin.login');
    Route::post('/dashboard', [DashboardController::class, 'welcome'])->name('admin.dashboard');

    Route::middleware('auth')->group(function () {

        Route::get('/categories', [App\Http\Controllers\CategoryController::class, 'categories'])->name('admin.categories.index');
        Route::get('/products', [App\Http\Controllers\ProductController::class, 'product'])->name('admin.product.index');
        Route::get('/offers',  [OfferController::class,'index'])->name('admin.offer.index');
        Route::get('/offers/{offer_id}',  [OfferController::class,'show'])->name('admin.offer.show');

        Route::get('/orders',  [OrderController::class,'index'])->name('admin.orders.index');
        Route::get('/orders/{order_id}',  [OrderController::class,'show'])->name('admin.orders.show');

        Route::post('/orders/{order_id}',  [OrderController::class,'update'])->name('admin.orders.update');

        Route::get('/streams', [StreamController::class, 'index'])->name('stream.index');


        Route::resource('categories', CategoryController::class);
        Route::resource('products', AdminProductController::class)->except([
            'show'
        ]);

        Route::post('/stream', [StreamController::class,'store']);

        // Route::post('logout', [AdminAuthController::class, 'logout'])->name('logout');
        Route::prefix('dashboard')->group(function () {
            Route::get('/', [DashboardController::class, 'welcome']);
        });

        Route::get('/seller-money-requests',  [SellerMoneyRequestController::class,'index'])->name('admin.money-requests.index');

        Route::get('/money-requests/create',  [SellerMoneyRequestController::class,'create'])->name('admin.money-requests.create');
        Route::post('/money-requests',  [SellerMoneyRequestController::class,'store'])->name('admin.money-requests.store');
        Route::get('/money-requests/change-status/{request_id}/{status}',  [SellerMoneyRequestController::class,'changeStatus'])->name('admin.money-requests.change-status');

    });
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
