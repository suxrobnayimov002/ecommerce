<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::query();
        
        $statusColors = [
            'new' => 'info',
            'confirmed' => 'warning ',
            'declined' => 'danger',
            'closed' =>  'success',
            'delayed' => 'primary',
            'ready_deliver' => 'info',
        ];

        if(auth()->user()->isSeller){
            $orders = $orders->whereHas('stream', function($q){
                $q->where('user_id' , auth()->id());
            });
        }

        $orders = $orders->with('product')->orderByDesc('id')->get();

        return view('admin.order.index', [
            'orders' => $orders,
            'statusColors' => $statusColors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $input = $request->validate([
            'customer_name' => 'required',
            'phone_number' => 'required',
            'region_id' => 'required',
            'product_id' => 'required|exists:products,id',
            'stream_id' => 'nullable'
        ]);


        $input['status'] = Order::STATUS_NEW;

        if(auth()->user() && auth()->user()->isSeller){
            $input['customer_id'] = auth()->id();
        }

        $order = Order::create($input);

        if($order){
            return view('product.thankyou');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($order)
    {

        $order = Order::findOrFail($order);
        
        return view('admin.order.show', [
            'order' => $order,
            'statuses' =>  Order::$statuses
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order_id)
    {

        $input = $request->validate([
            'status' => 'required|in:new,closed,declined,confirmed,delayed,ready_deliver',
        ]);

        $order = Order::findOrFail($order_id);

        $orderUpdated = $order->update([
            'status' => $input['status']
        ]);

        if($orderUpdated){
            return redirect()->route('admin.orders.index');
        }

        abort(422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function myOrders()
    {
        $orders = Order::where('customer_id', auth()->id())->with('product','region')->get();

        return view('admin.order.myorders', [
            'orders' => $orders,
        ]);
    }
}
