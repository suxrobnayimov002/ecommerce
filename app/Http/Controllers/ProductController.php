<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Category;
use App\Models\Region;



class ProductController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();

        $input = $request->validate([
            'category_id' => 'nullable|exists:categories,id',
            'product_name' => 'nullable',
            'order_by' => 'nullable'
        ]);

        $query = Product::query();

        if(isset($input['category_id'])){
            $query = $query->where('category_id', $input['category_id']);
        }

        if(isset($input['product_name'])){
            $query = $query->where('name','ilike', '%'.$input['product_name'].'%');
        }

        if(isset($input['order_by'])){
            $query = $query->orderBy($input['order_by']);
        }else{
            $query = $query->orderByDesc('created_at');
        }

        $products = $query->where('is_published', true)->get();

        return view('product.index', compact('products', 'categories'));
    }

    public function show($id)
    {
        // $item = $this->repo->findById($id);
        $product = Product::findOrFail($id);
        $regions = Region::get();
        $similar_products = [];

        if($product){
            $similar_products = Product::where('category_id', $product->category_id)->where('id','!=', $product->id)->get();
        }

        return view('product.show', [
            'products' => $product,
            'similar_products' => $similar_products,
            'regions' => $regions
        ]);
    }


}
