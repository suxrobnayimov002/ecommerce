<?php

namespace App\Http\Controllers;

use App\Models\SellerMoneyRequest;
use App\Http\Requests\StoreSellerMoneyRequestRequest;
use App\Http\Requests\UpdateSellerMoneyRequestRequest;
use Illuminate\Http\Request;


class SellerMoneyRequestController extends Controller
{
    public function __construct(SellerMoneyRequest $model){}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SellerMoneyRequest::query();

        if ($request->has('status')) {
            $query->where('status', $request->get('status'));
        }

        if ($request->has('card_number')) {
            $query->where('card_number','like', '%'.$request->get('card_number').'%');
        }

        if(auth()->user()->isSeller){
            $query->where('seller_id', auth()->id());
        }
        

        $requests = $query->with('seller')->paginate(20);

        $statusColors = [
            1 => 'info',
            2 => 'danger',
            3 =>  'success'
        ];


        return view('admin.money-requests.index', [
            'requests' => $requests,
            'statusColors' => $statusColors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.money-requests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSellerMoneyRequestRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSellerMoneyRequestRequest $request)
    {
        $input = $request->validated();

        $input['seller_id'] = auth()->id();
        $input['status'] = SellerMoneyRequest::STATUS_NEW;

        $model = SellerMoneyRequest::create($input);

        if($model){
            return redirect()->route('admin.money-requests.index');
        }

        abort(422);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SellerMoneyRequest  $sellerMoneyRequest
     * @return \Illuminate\Http\Response
     */
    public function show(SellerMoneyRequest $sellerMoneyRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SellerMoneyRequest  $sellerMoneyRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(SellerMoneyRequest $sellerMoneyRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSellerMoneyRequestRequest  $request
     * @param  \App\Models\SellerMoneyRequest  $sellerMoneyRequest
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSellerMoneyRequestRequest $request, SellerMoneyRequest $sellerMoneyRequest)
    {
        //
    }

    public function changeStatus($modelID,$status)
    {
        if(!auth()->user()->isAdmin){
            abort(403);
        }
        
        $model = SellerMoneyRequest::findOrFail($modelID);

        $input['status']  = $status;

        if($model->update($input)){
            return redirect()->route('admin.money-requests.index');
        }

        abort(422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SellerMoneyRequest  $sellerMoneyRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(SellerMoneyRequest $sellerMoneyRequest)
    {
        //
    }
}
