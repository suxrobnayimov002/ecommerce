<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Stream;
use App\Models\Region;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StreamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        if($user && $user->isSeller) {
            $streams = Stream::where('user_id', $user->id)->with('product')->get();
            return view('stream.index', compact('streams'));
        }
        // $streams = Stream::all();

        abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required',
            'product_id' => 'required:exists:products,id',
        ]);

        $input['uuid'] = Str::random(8);
        $input['user_id'] = auth()->id();

        $stream = Stream::create($input);

        if($stream){
            return [
                'link' => config('app.stream_base_url').$stream->uuid
            ];
        }

        abort(422);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        $stream = Stream::where('uuid', $uuid)->with('product')->first();
        $regions = Region::get();

        // $products = Product::findOrFail($id);
        // $similar_products = [];

        // if($product){
        //     $similar_products = Product::where('category_id', $products->category_id)->where('id','!=', $products->id)->get();
        // }

        if($stream && $stream->product)
        {
            return view('stream.show', [
                'product' => $stream->product,
                'regions' => $regions,
                'stream' => $stream,
                // 'similar_products' => $similar_products
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function edit(Stream $stream)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stream $stream)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stream  $stream
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stream $stream)
    {
        //
    }
}
