<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Products\StoreProduct;
use App\Http\Requests\Products\UpdateProduct;

use App\Repositories\Admin\ProductRepository;

use App\Models\Product;
use App\Models\Category;


class ProductController extends Controller
{

    protected $repo;

    public function __construct(ProductRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        $input = $request->validate([
            'category_id' => 'nullable|exists:categories,id',
            'product_name' => 'nullable',
            'order_by' => 'nullable'
        ]);

        $query = Product::query();

        if(isset($input['category_id'])){
            $query = $query->where('category_id', $input['category_id']);
        }

        if(isset($input['product_name'])){
            $query = $query->where('name','ilike', '%'.$input['product_name'].'%');
        }

        if(isset($input['order_by'])){
            $query = $query->orderBy($input['order_by']);
        }else{
            $query = $query->orderByDesc('created_at');
        }

        $products = $query->get();

        return view('admin.product.index', compact('products'));

    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create', [
            'categories' => $categories
        ]);
    }

    public function store(StoreProduct $request)
    {

        $this->repo->store($request->all());


        return redirect()->route('products.index');
    }

    public function show($id)
    {
        // $item = $this->repo->findById($id);
        $product = Product::findOrFail($id);

        $similar_products = [];

        if($product){
            $similar_products = Product::where('category_id', $product->category_id)->where('id','!=', $product->id)->get();
        }

        return view('product.show', [
            'products' => $product,
            'similar_products' => $similar_products
        ]);
    }

    public function edit($id)
    {
        $product = $this->repo->findById($id);
        $categories = Category::all();

        return view('admin.product.edit', compact('product', 'categories'));
    }

    public function update(UpdateProduct $request, $id)
    {
        $this->repo->update($request->all(), $id);
        return redirect()->route('products.index');
    }

    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        return redirect()->route('products.index')
                        ->with('success','list deleted successfully');
    }
}
