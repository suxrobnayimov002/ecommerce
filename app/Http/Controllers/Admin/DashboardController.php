<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Report;

class DashboardController extends Controller
{
    public function welcome(){
        
        $user = auth()->user();
        
        if($user->isSeller){
            $statistics = Report::sellerStatistic(auth()->id());
        }else{
            $statistics = Report::sellerStatistic();    
        }
        
        if($user && $user->isOperator){
            return redirect()->route('admin.orders.index');
        }

        return view('admin.dashboard', [
            'user' => $user,       
            'statistics' => $statistics    
        ]);
    }
}
