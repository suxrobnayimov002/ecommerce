<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class OfferController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();
        
        $input = $request->validate([
            'category_id' => 'nullable|exists:categories,id',
            'product_name' => 'nullable',
            'order_by' => 'nullable'
        ]);

        $query = Product::query();

        if(isset($input['category_id'])){
            $query = $query->where('category_id', $input['category_id']);
        }

        if(isset($input['product_name'])){
            $query = $query->where('name','ilike', '%'.$input['product_name'].'%');
        }

        if(isset($input['order_by'])){
            $query = $query->orderBy($input['order_by']);
        }else{
            $query = $query->orderByDesc('created_at');
        }

        $products = $query->active()->paginate(20);
        

        return view('admin.offer.index',  compact('products', 'categories'));
    }

    public function show($id)
    {
        $offer = Product::findOrFail($id);

        return view('admin.offer.show', compact('offer'));
    }



}
