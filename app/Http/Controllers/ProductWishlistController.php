<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Products\StoreProduct;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductWishlist;


class ProductWishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $products = ProductWishlist::where('customer_id', $user->id)->with('product')->get();

        return view('favorite.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($product_id)
    {
        // $input = $request->validate([
        //     'product_id' => 'required|exists:products,id',
        // ]);

        $input['product_id'] = $product_id;
        $input['customer_id'] = auth()->id();


        $wishlist = ProductWishlist::firstOrCreate($input);

        return $wishlist;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productDeleted = ProductWishlist::findOrFail($id)->delete();

        if($productDeleted){

            return $productDeleted;
        }

        abort(422);
    }
}
