<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;

class CategoryController extends Controller
{
    public function categorie(){

        $categories = Category::orderByDesc('created_at')->paginate(20);

        return view('admin.categories.index', compact('categories'));

    }
}
