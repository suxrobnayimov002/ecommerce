<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Product;

class HomeController extends Controller
{
    public function home()
    {
        $categories = Category::withCount('products')->get();
        
        $products = Product::orderByDesc('price')->where('is_published', true)->paginate(4);

        $last_products = Product::orderByDesc('created_at')->where('is_published', true)->take(4)->get();

        return view('welcome', compact('products', 'categories', 'last_products'));
    }

    public function contact()
    {
        return view('contact.index');
    }
}
