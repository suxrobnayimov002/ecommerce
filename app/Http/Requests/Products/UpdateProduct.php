<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'image' => 'nullable',
            'price' => 'required',
            'profit' => 'required',
            'is_published' => 'required',
            'advertising_link' => 'required',
            'coin' => 'required',
            // 'customers_show_count' => 'required'
        ];
    }
}
