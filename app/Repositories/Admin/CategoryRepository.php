<?php

namespace App\Repositories\Admin;

use App\Models\Category;
use MediaUploader;

//use Your Model

/**
 * Class categoriesRepository.
 */
class CategoryRepository
{
    protected $entity;

    public $media = null;

    public function __construct(Category $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $items = $this->entity->paginate(10);
       return $items;
    }

    public function findById($id)
    {
        $item = $this->entity->where('id', $id)->get();
        return $item;
    }

    public function store($request)
    {
        $item = $this->entity->create([
            'name' => $request['name'],
        ]);

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item);
        }

        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item[0]);
        }
        // dd($item[0]);
        $item[0]->update([
            'name' => $request['name'],
        ]);

        return $item;
    }

    public function fileUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('categories')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['categories_image']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'image' => '/storage/categories/' . $item->getMedia('categories_image')->first()->filename . '.' . $item->getMedia('categories_image')->first()->extension
                ]
            );
        }
    }
}
