<?php

namespace App\Repositories\Admin;

use App\Models\Product;
use MediaUploader;
use Illuminate\Support\Arr;
//use Your Model

/**
 * Class ProductsRepository.
 */
class ProductRepository
{
    protected $entity;

    public $media = null;

    public function __construct(Product $entity)
    {
        $this->entity = $entity;
    }

    public function all()
    {
       $items = $this->entity->paginate(10);
       return $items;
    }

    public function findById($id)
    {
        return $this->entity->findOrFail($id);
    }

    public function store($request)
    {

        $item = $this->entity->create(Arr::except($request, ['image']));

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item);
        }
        return $item;
    }

    public function update($request, $id)
    {
        $item = $this->findById($id);

        if (isset($request['image'])) {
            $this->fileUpload($request['image'], $item);
        }

        $item->update(Arr::except($request, ['image']));

        return $item;
    }

    public function fileUpload($image, $item)
    {
        $this->media = MediaUploader::fromSource($image)->useFilename(time())->toDirectory('product')->upload();

        if (!is_null($this->media)) {
            $item->syncMedia($this->media->id, ['product_image']);
            return $this->entity->where('id', $item->id)->update(
                [
                    'image' => '/storage/product/' . $item->getMedia('product_image')->first()->filename . '.' . $item->getMedia('product_image')->first()->extension
                ]
            );
        }
    }
}
