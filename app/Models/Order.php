<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Product;
use App\Models\Region;


class Order extends Model
{
    use HasFactory;

    public const STATUS_NEW = 'new';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_READY_FOR_DELIVER = 'ready_deliver';
    public const STATUS_DELAYED = 'delayed';
    public const STATUS_CLOSED = 'closed';
    public const STATUS_DECLINED = 'declined';


    public static $statuses = [
        'new' => 'Yangi buyurtma',
        'confirmed' => 'Yetkazilmoqda',
        'declined' => 'Bekor qilindi',
        'closed' =>  'Yetkazildi',
        'ready_deliver' => 'Dostavkaga tayyor',
        'delayed' => 'Keyin oladi'
    ];

    protected $fillable = [
        'stream_id',
        'product_id',
        'customer_id',
        'customer_name',
        'phone_number',
        'region_id',
        'status'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function stream()
    {
        return $this->belongsTo(Stream::class);
    }
    
    public function getStatusLabelAttribute()
    {
       return self::$statuses[$this->status];
    }
}
