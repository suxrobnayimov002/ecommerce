<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellerMoneyRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'card_number',
        'amount',
        'status',
        'seller_id'
    ];

    
    public const STATUS_NEW = 1;
    public const STATUS_DECLINED = 2;
    public const STATUS_CONFIRMED = 3;
    

    public function getstatusLabelAttribute()
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                return 'Yangi';
                break;
            case self::STATUS_DECLINED:
                return 'Rad etilgan';
                break;
            case self::STATUS_CONFIRMED:
                return 'Tasdiqlangan';
                break;
        }

        return '';
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id','id');
    }
}
