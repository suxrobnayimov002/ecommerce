<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Product extends Model
{
    use HasFactory, Mediable;

    protected $fillable = [
        'category_id',
        'name',
        'description',
        'image',
        'price',
        'profit',
        'is_published',
        'advertising_link',
        'coin'
        // 'customers_show_count'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getImageAttribute($value)
    {
        return ($value) ? (gettype($value) != 'string' ) ? stream_get_contents($value) : $value     : '';
    }

    public function scopeActive($query)
    {
        return $query->where('is_published', true);
    }
}
