<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Report extends Model
{
    use HasFactory;

    public static function sellerStatistic($seller_id = null)
    {
        $statistics = DB::table('orders')
        ->selectRaw("sum(profit) as profit, sum(coin) as coin, count(*) as transactions")
        ->leftJoin('products', 'orders.product_id', '=', 'products.id')
        ->leftJoin('streams', 'orders.stream_id', '=', 'streams.id')
        ->where('orders.status', 'closed');

        if($seller_id) {
            $statistics = $statistics->where('streams.user_id', $seller_id);
        }

        $statistics = $statistics->first();
        
        $streamCount = DB::table('streams');
        
        if($seller_id){
           $streamCount = $streamCount->where('user_id', $seller_id);
        }
        
        $streamCount = $streamCount->count();

        return [
            'coin' => $statistics->coin ?? 0,
            'profit' => $statistics->profit ?? 0,
            'streams' => $streamCount,
            'transactions' => $statistics->transactions ?? 0
        ];

    }
}
