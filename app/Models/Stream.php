<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;


class Stream extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'product_id',
        'user_id',
        'uuid',
        'link'
    ];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getFullUrlAttribute()
    {
        return config('app.stream_base_url').$this->uuid;
    }
}
