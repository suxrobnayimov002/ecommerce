<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Category extends Model
{
    use HasFactory;
    use Mediable;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'image'
    ];

    public function getImageAttribute($value)
    {
        return ($value) ? (gettype($value) != 'string' ) ? stream_get_contents($value) : $value     : '';
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
